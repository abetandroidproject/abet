package com.example.abeete3.network

import androidx.room.Entity

@Entity(tableName = "firstAid")
data class FirstAidInfo(    val id: Long,
                            val title: String,
                            val medication:String,
                            val sympthoms:String,
                            val image: ByteArray,
                            val video: ByteArray
)
