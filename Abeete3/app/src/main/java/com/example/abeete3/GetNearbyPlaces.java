package com.example.abeete3;

import android.os.AsyncTask;
import com.google.android.gms.maps.GoogleMap;

import java.io.IOException;

public class GetNearbyPlaces extends AsyncTask<Object,String,String> {
    String googlePlacesData;
    GoogleMap mMap;
    String  url;

    @Override
    protected String doInBackground(Object... objects) {
        mMap = (GoogleMap)objects[0];
        url = (String)objects[1];

        DownloadUrl downloadUrl=new DownloadUrl();
        try {
            googlePlacesData=downloadUrl.readUrl(url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return googlePlacesData;
    }


    @Override
    protected void onPostExecute(String s){
        super.onPostExecute(s);
    }
}
