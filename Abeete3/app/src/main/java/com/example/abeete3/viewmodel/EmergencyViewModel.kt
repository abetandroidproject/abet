package com.example.abeete3.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import androidx.lifecycle.*
import com.example.abeete3.data.AbetDatabase

import com.example.abeete3.data.EmergencyContact
import com.example.abeete3.network.EmergencyApiService

import com.example.abeete3.repository.EmergencyRepository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response


class EmergencyViewModel(application:Application):AndroidViewModel(application) {
    private val emergencyRepository:EmergencyRepository
    val allContacts : LiveData<EmergencyContact>
    private val filterData = MutableLiveData<String>()
    private lateinit var searchData: LiveData<EmergencyContact>

    init {
        val dao = AbetDatabase.getDatabase(application).emergencyDao()
        val apiService = EmergencyApiService.getInstance()
        emergencyRepository = EmergencyRepository(dao, apiService)
        if(connected(getApplication())){
            viewModelScope.launch (Dispatchers.IO){
                emergencyRepository.allContactsApi()
            }
        }
        allContacts = emergencyRepository.allContacts()
    }


    fun insertContact(contact:EmergencyContact)=viewModelScope.launch (Dispatchers.IO){
        //Log.d("Add", group.groupName)
        if(connected(getApplication())){
           emergencyRepository.insertContactApi(contact)
        }
    }

    fun deleteUser(contact:EmergencyContact)=viewModelScope.launch (Dispatchers.IO) {
        if(connected(getApplication())){
           emergencyRepository.deleteContactApi(contact)
        }
    }

    fun updateUser(contact:EmergencyContact) = viewModelScope.launch (Dispatchers.IO){
        if(connected(getApplication())){
            emergencyRepository.updateContactApi(contact)
        }
    }

    fun getContactByName(name: String):LiveData<EmergencyContact>{
        filterData.value=name
        searchData= Transformations.switchMap(filterData){
            if (connected(getApplication())){
                viewModelScope.launch(Dispatchers.IO) {
                   emergencyRepository.findByName(it)
                }
               emergencyRepository.getContactByName(it)
            }else{
                emergencyRepository.getContactByName(it)
            }

        }
        return searchData

    }


    private fun connected(context: Context):Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }





}