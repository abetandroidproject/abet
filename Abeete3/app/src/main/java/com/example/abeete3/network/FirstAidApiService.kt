package com.example.abeete3.network
import com.example.abeete3.data.FirstAid
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
interface FirstAidApiService {
    @GET("info/all")
    fun findAllAsync(): Deferred<Response<List<FirstAid>>>
    @GET("info/{id}")
    fun findByIdAsync(@Path("id") id: Long): Deferred<Response<FirstAid>>
    @GET("info")
    fun findByTitleAsync(@Query("title") title: String): Deferred<Response<FirstAid>>
    @POST("info")
    fun insertInfoAsync(@Body newInfo: FirstAid): Deferred<Response<Void>>
    @PUT("info/{id}")
    fun updateInfoAsnc(@Path("id") id: Long, @Body newInfo: FirstAid): Deferred<Response<Void>>
    @DELETE("info/{id}")
    fun deleteInfoAsync(@Path("id") id: Long): Deferred<Response<Void>>


    companion object {

        private val baseUrl = "http://192.168.127.1:7080/"

        fun getInstance():FirstAidApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(FirstAidApiService::class.java)
        }
    }
}