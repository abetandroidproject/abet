package com.example.abeete3.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import androidx.lifecycle.*
import com.example.abeete3.data.AbetDatabase

import com.example.abeete3.data.User
import com.example.abeete3.network.UserApiService

import com.example.abeete3.repository.UserRepository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response


class UserViewModel(application:Application):AndroidViewModel(application) {
    private val userRepository:UserRepository
    val allUsers : LiveData<User>
    private val filterData = MutableLiveData<String>()
    private lateinit var searchData: LiveData<User>

    init {
        val dao = AbetDatabase.getDatabase(application).userDao()
        val apiService = UserApiService.getInstance()
        userRepository = UserRepository(dao, apiService)
        if(connected(getApplication())){
            viewModelScope.launch (Dispatchers.IO){
                userRepository.allUsersApi()
            }
        }
        allUsers = userRepository.allUsers()
    }


    fun insertUser(user:User)=viewModelScope.launch (Dispatchers.IO){
        Log.d("Add", user.name)
        if(connected(getApplication())){
            userRepository.insertUserApi(user)
        }
    }

    fun deleteUser(user:User)=viewModelScope.launch (Dispatchers.IO) {
        if(connected(getApplication())){
            userRepository.deleteUserApi(user)
        }
    }

    fun updateUser(user:User) = viewModelScope.launch (Dispatchers.IO){
        if(connected(getApplication())){
            userRepository.updateUserApi(user)
        }
    }

    fun getUserByName(name: String):LiveData<User>{
        filterData.value=name
        searchData= Transformations.switchMap(filterData){
            if (connected(getApplication())){
                viewModelScope.launch(Dispatchers.IO) {
                   userRepository.findByName(it)
                }
               userRepository.getUserByName(it)
            }else{
                userRepository.getUserByName(it)
            }

        }
        return searchData

    }


    private fun connected(context: Context):Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }





}