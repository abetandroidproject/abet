package com.example.abeete3.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.example.abeete3.data.FirstAid



@Dao
interface FirstAidDao {
    @Query("SELECT * FROM FirstAid ORDER BY title")
    fun getAllInfos(): LiveData<FirstAid>

    @Query("SELECT * FROM FirstAid WHERE title=:title")
    fun getInfo(title:String):LiveData<FirstAid>
}