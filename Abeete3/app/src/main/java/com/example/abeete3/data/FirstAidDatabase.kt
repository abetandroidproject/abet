package com.example.abeete3.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(User::class), version=1)
abstract class FirstAidDatabase :RoomDatabase(){

    abstract fun firstAidInfo():FirstAidDao
    companion object {
        @Volatile
        private var INSTANCE: FirstAidDatabase? =null
        fun getDatabase(context: Context): FirstAidDatabase {
            val  tempInstance= INSTANCE
            if(tempInstance!=null){
                return tempInstance

            }
            synchronized(this)
            {
                val instance= Room.databaseBuilder(context.applicationContext, FirstAidDatabase::class.java,"user_database").build()
                INSTANCE=instance
                return instance
            }

        }
    }
}