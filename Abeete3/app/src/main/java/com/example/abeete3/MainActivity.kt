package com.example.abeete3

import android.app.ActivityManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.AsyncTask
import android.os.Bundle
import android.os.IBinder
import android.provider.ContactsContract
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.example.abeete3.data.User
import com.example.abeete3.fragment.eight
import com.example.abeete3.fragment.getBackgroundNotification

import com.example.abeete3.fragment.seven
import com.example.abeete3.viewmodel.UserViewModel

import com.example.abeete3.viewmodel.FirstAidViewModel

import kotlinx.android.synthetic.main.activity_main.*
import java.lang.IllegalArgumentException

class MainActivity : AppCompatActivity(), seven.RegisterButtonSelected {
    lateinit var  userViewModel: UserViewModel



    override fun getUserByName(name: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun notNowButtonSelected() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var textMessage: TextView

   // private val viewModel2 = ViewModelProviders.of(this).get(FirstAidViewModel::class.java)
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController

    var myService: ExampleService?=null
    var isBound = false
//private val viewModel by lazy{ViewModelProviders.of(this).get(UserViewModel::class.java)}
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding : com.example.abeete3.databinding.ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)

       // binding.profile=userViewModel

        //binding.fromApi = viewModel2
        //binding.fromApi1 = viewModel2
       // binding.fromApi2 = viewModel2



        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        setupBottomNavMenu(navController)

        val serviceClass = ExampleService::class.java
        val serviceIntent = Intent(applicationContext, serviceClass)

        if(!isServiceRunning(serviceClass))
        {
            startService(serviceIntent)
            bindService(serviceIntent, myConnection, Context.BIND_AUTO_CREATE)
        }else{
            bindService(serviceIntent, myConnection, Context.BIND_AUTO_CREATE)
        }

    }

    private fun setupBottomNavMenu(navController: NavController)
    {
        nav_view?.let{
            NavigationUI.setupWithNavController(it,navController)
        }
    }



    private val myConnection=object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            val binder = p1 as ExampleService.MyLocalBinder
            myService = binder.getService()
            isBound = true

            getBackgroundNotification(applicationContext, myService).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)

        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            isBound = false
        }
    }
    fun showTime()
    {
        val currentTime = myService?.getCurrentTime()
    }
    override fun onDestroy() {

        val serviceClass = ExampleService::class.java
        val servicIntent = Intent(applicationContext, serviceClass)
        try {
            unbindService(myConnection)
        }catch (e: IllegalArgumentException)
        {
            Log.w("main", "error")
        }
        if(isServiceRunning(ExampleService::class.java))
        {
            stopService(servicIntent)
        }
        super.onDestroy()
    }

    private fun isServiceRunning(serviceClass: Class<*>):Boolean
    {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        for (i in activityManager.getRunningServices(Integer.MAX_VALUE))
        {
            if(serviceClass.name == i.service.className)
            {
                return true
            }
        }
        return false
    }

}
