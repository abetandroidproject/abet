package com.example.abeete3.network
import com.example.abeete3.data.User
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface UserApiService {
    @GET("user/all")
    fun findAllAsync(): Deferred<Response<List<User>>>
    @GET("user/{name}")
    fun findByNameAsync(@Path("name") name: String): Deferred<Response<User>>
    @POST("user")
    fun insertUserAsync(@Body user: User): Deferred<Response<Void>>
    @PUT("user/{name}")
    fun updateUserAsnc(@Path("name") name: String, @Body user: User): Deferred<Response<Void>>
    @DELETE("user/{name}")
    fun deleteUserAsync(@Path("name") name: String): Deferred<Response<Void>>


    companion object {

        private val baseUrl = "http://192.168.127.1:7080/"

        fun getInstance():UserApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .connectTimeout(20, TimeUnit.MINUTES)
               // .readTimeout(100000000, TimeUnit.SECONDS)
               // .writeTimeout(1000000, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(UserApiService::class.java)
        }
    }
}