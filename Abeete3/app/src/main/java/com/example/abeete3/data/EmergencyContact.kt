package com.example.abeete3.data


import androidx.room.Entity
import androidx.room.PrimaryKey

import java.io.Serializable

@Entity
data class EmergencyContact(
                             @PrimaryKey val name: String,
                    val phoneNumber:String,
                    val relationShip:String

):Serializable
