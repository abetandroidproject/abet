package com.example.abeete.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.abeete3.data.EmergencyContact
import com.example.abeete3.data.User


@Dao
interface EmergencyDao {

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    fun insertContact(contact: EmergencyContact):Long

    @Query("SELECT * FROM EmergencyContact WHERE name=:name")
    fun getContact(name:String):LiveData<EmergencyContact>

    @Query("SELECT * FROM EmergencyContact")
    fun allContacts():LiveData<EmergencyContact>


}