package com.example.abeete3

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Binder
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import java.text.SimpleDateFormat
import java.util.*

class ExampleService :Service(){
    private val myBinder = MyLocalBinder()

    override fun onBind(p0: Intent?): IBinder? {
        return myBinder
    }

    fun getCurrentTime():String
    {
        val dateformat = SimpleDateFormat("HH:mm:ss MM/dd/yy", Locale.US)
        return dateformat.format(Date())
    }

    inner class MyLocalBinder: Binder()
    {
        fun getService():ExampleService
        {
            return this@ExampleService
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        getNotification(applicationContext)
        return Service.START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        stopForeground(true)
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        stopSelf()
    }

    companion object
    {
        const val CHANNEL_ID ="SDFGHJKL"
        const val CHANNEL_NAME ="Sample Notification"
    }

    private fun createChannel(context: Context)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            val notificationManager=context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance)
            notificationChannel.enableVibration(true)
            notificationChannel.setShowBadge(true)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.parseColor("#e8334a")
            notificationChannel.description ="hjjgjhgviyt iuihv uygvyt"
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    private  fun getNotification(context: Context)
    {
        createChannel(context)
        var mNotification: Notification
        var notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notifyIntent = Intent(context, MainActivity::class.java)

        val title="Sample Notification"
        val message = "sdkfjbhskjdbsbh sdfkhbsdjh sdkjbs"
        notifyIntent.putExtra("title", title)
        notifyIntent.putExtra("message", message)
        notifyIntent.putExtra("notification", true)
        notifyIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        val pendingInt = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            mNotification = Notification.Builder(context, CHANNEL_ID)
                .setContentIntent(pendingInt)
                .setSmallIcon(R.drawable.notification_template_icon_low_bg)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setStyle(Notification.BigTextStyle().bigText(message))
                .setContentText(message).build()
        }else
        {
            mNotification = Notification.Builder(context)
                .setContentIntent(pendingInt)
                .setSmallIcon(R.drawable.notification_template_icon_low_bg)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(title)
                .setStyle(Notification.BigTextStyle().bigText(message))
                .setContentText(message).build()
        }
        startForeground(999, mNotification)
    }
}