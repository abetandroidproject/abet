package com.example.abeete.data

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.abeete3.data.User


@Dao
interface UserDao {

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    fun insertUser(user:User):Long

    @Query("SELECT * FROM user ORDER BY name")
    fun AllUsers():LiveData<User>

    @Query("Select * from user where name=:name")
    fun getUserByName(name:String):LiveData<User>


}