package com.example.abeete3.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import androidx.lifecycle.*
import com.example.abeete3.data.AbetDatabase
import com.example.abeete3.data.FirstAid

import com.example.abeete3.data.User
import com.example.abeete3.network.FirstAidApiService
import com.example.abeete3.network.UserApiService
import com.example.abeete3.repository.FirstAidRepository

import com.example.abeete3.repository.UserRepository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class FirstAidViewModel(application:Application):AndroidViewModel(application) {
    private val firstAidRepository:FirstAidRepository
    val allInfos : LiveData<FirstAid>
    private val filterData = MutableLiveData<String>()
    private lateinit var searchData: LiveData<FirstAid>

    init {
        val dao = AbetDatabase.getDatabase(application).firstAidDao()
        val apiService = FirstAidApiService.getInstance()
       firstAidRepository = FirstAidRepository(dao, apiService)
        if(connected(getApplication())){
            viewModelScope.launch (Dispatchers.IO){
                firstAidRepository.allInfoApi()
            }
        }
        allInfos = firstAidRepository.allInfo()
    }



    fun getInfoByTitle(title: String):LiveData<FirstAid>{
        filterData.value=title
        searchData= Transformations.switchMap(filterData){
            if (connected(getApplication())){
                viewModelScope.launch(Dispatchers.IO) {
                    firstAidRepository.findByTitle(title)
                }
               firstAidRepository.getInfoByTitle(it)
            }else{
                firstAidRepository.getInfoByTitle(it)
            }

        }
        return searchData

    }

    private fun connected(context: Context):Boolean {

        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }





}