package com.example.abeete3.repository

import android.util.Log
import androidx.lifecycle.LiveData
import retrofit2.Response

import com.example.abeete.data.UserDao
import com.example.abeete3.data.User
import com.example.abeete3.network.UserApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepository(private val userDao: UserDao, private val userApiService: UserApiService) {

    fun insertUser(user:User):Long{
        return userDao.insertUser(user)
    }
    fun getUserByName(name:String):LiveData<User>{
    return userDao.getUserByName(name)
     }

    fun allUsers():LiveData<User>{
        return userDao.AllUsers()
    }
    suspend fun allUsersApi(){
        val response: Response<List<User>> = userApiService.findAllAsync().await()
        val users = response.body()
        if(users != null){
            Log.d("Repo all" , users[0].name)
            for(user in users){
                userDao.insertUser(user)
            }
        }
    }

    suspend fun insertUserApi(user:User){
        Log.d("Repo", user.name)
        withContext(Dispatchers.IO) {
            userApiService.insertUserAsync(user).await()
        }
    }

    fun deleteUserApi(user:User){
        userApiService.deleteUserAsync(user.name)
    }

    fun updateUserApi(user:User){
        userApiService.updateUserAsnc(user.name,user)
    }

    suspend fun findByName(name:String){
        val response: Response<User> = userApiService.findByNameAsync(name).await()
        val user = response.body()
        if(user != null){
            userDao.insertUser(user)
        }
    }


}
