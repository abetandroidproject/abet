package com.example.abeete3

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class FirstAidAdapter (fragmentManager: FragmentManager): FragmentStatePagerAdapter(fragmentManager){

    var mfm = fragmentManager
    var mFragmentItem:ArrayList<Fragment> = ArrayList()
    var mFragmentTitles:ArrayList<String> = ArrayList()

    fun addFragment(fragmentItem:Fragment, fragmentTitle:String)
    {
        mFragmentTitles.add(fragmentTitle)
        mFragmentItem.add(fragmentItem)
    }

    override fun getItem(position: Int): Fragment {
        return mFragmentItem[position]
    }

    override fun getCount(): Int {
        return mFragmentItem.size
    }
    override fun getPageTitle(position: Int): String {
        return mFragmentTitles[position]
    }
}