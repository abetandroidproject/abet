package com.example.abeete3.fragment


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.abeete3.R
import com.example.abeete3.data.User
import com.example.abeete3.databinding.FragmentProfileBinding
import com.example.abeete3.databinding.FragmentRegistrationBinding
import com.example.abeete3.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_registration.*
import java.io.ByteArrayOutputStream


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class eight : Fragment() {
private lateinit var userViewModel:UserViewModel

    private lateinit var binding: FragmentProfileBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentProfileBinding.inflate(inflater, container, false)
        userViewModel=ViewModelProviders.of(this).get(UserViewModel::class.java)



        userViewModel.allUsers.observe(this, Observer{
                user->user?.let {
            binding.FullName.setText(user.name)
            binding.City.setText(user.city)
            binding.HouseNo.setText(user.houseno)
            binding.dOB.setText(user.dob)

            binding.Weight.setText(user.weight.toString())
            binding.Heigh.setText(user.height.toString())
            binding.Allergy.setText(user.allergies)
            binding.MedicalConditions.setText(user.medicalConditions)
            binding.Medications.setText(user.medications)
        }
        })
        binding.registerButton.setOnClickListener {
            binding?.let {
                userViewModel.updateUser(readFields(binding))

            }
            binding.FullName.setText("")
            binding.City.setText("")
            binding.HouseNo.setText("")
            binding.Heigh.setText("")
            binding.Weight.setText("")

            binding.dOB.setText("")
            binding.Allergy.setText("")
            binding.MedicalConditions.setText("")
            binding.Medications.setText("")
        }

        return binding.root


    }
    private fun readFields(binding: FragmentProfileBinding) : User {
        return User(
            binding.FullName.text.toString(),
            binding.City.text.toString(),
            binding.HouseNo.text.toString(),
            binding.Heigh.text.toString().toDouble(),
            binding.Weight.text.toString().toDouble(),
            binding.BloodType.selectedItem.toString(),
            binding.dOB.text.toString(),
            binding.Allergy.text.toString(),
            binding.MedicalConditions.text.toString(),
            binding.Medications.text.toString()

        )}


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
    }




