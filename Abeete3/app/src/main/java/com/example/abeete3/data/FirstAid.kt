package com.example.abeete3.data


import androidx.room.Entity
import androidx.room.PrimaryKey

import java.io.Serializable

@Entity
data class FirstAid(@PrimaryKey val id: Long,
                    val title: String,
                    val medication:String,
                    val sympthoms:String,
                    val image: String,
                    val video: String
):Serializable
