package com.example.abeete3.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.example.abeete3.FirstAidAdapter
import com.example.abeete3.R
import com.example.abeete3.databinding.FragmentFirstaidinfoBinding
import com.example.abeete3.databinding.FragmentProfileBinding
import com.example.abeete3.viewmodel.FirstAidViewModel
import com.example.abeete3.viewmodel.UserViewModel
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_firstaidinfo.*
import kotlinx.android.synthetic.main.fragment_firstaidinfo.view.*
import kotlinx.android.synthetic.main.fragment_text.view.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class five : Fragment() {

    lateinit var Tabs: TabLayout
    lateinit var viewPager: ViewPager
    lateinit var pagerAdapter: FirstAidAdapter
    private lateinit var firstAidViewModel: FirstAidViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentFirstaidinfoBinding.inflate(inflater, container, false)
        Tabs = binding.tabs
        viewPager = binding.viewpager

        firstAidViewModel= ViewModelProviders.of(this).get(FirstAidViewModel::class.java)
        val searchText=search.query.toString()
        search.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                //var info=firstAidViewModel.getInfoByTitle(searchText)

                      /* info.observe(FragmentFirstaidinfoBinding., Observer {

                        })*/

                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pagerAdapter = FirstAidAdapter(activity!!.supportFragmentManager)
        pagerAdapter!!.addFragment(Text(), "Texts")
        pagerAdapter!!.addFragment(Image(), "Images")
        pagerAdapter!!.addFragment(Video(), "Videos")

        viewPager.adapter=pagerAdapter
        Tabs.setupWithViewPager(viewPager)

    }

}
