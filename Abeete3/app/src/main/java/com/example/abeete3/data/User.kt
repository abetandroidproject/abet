package com.example.abeete3.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName="user")
data class User(
    @PrimaryKey @ColumnInfo(name="name")val name:String,
  //  @ColumnInfo(typeAffinity = ColumnInfo.BLOB) val image: ByteArray,
    @ColumnInfo(name="city") val city:String,
    @ColumnInfo(name="houseno") val houseno:String,
    @ColumnInfo(name="height") val height:Double,
    @ColumnInfo(name="weight") val weight:Double,
    @ColumnInfo(name="bloodType") val bloodType:String,
    @ColumnInfo(name="dob") val dob:String,
    @ColumnInfo(name="allergies")val allergies:String,
    @ColumnInfo(name="medicalConditions") val medicalConditions:String,
    @ColumnInfo(name="medications") val medications:String):Serializable



