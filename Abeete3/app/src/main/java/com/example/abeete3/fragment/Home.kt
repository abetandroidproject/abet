package com.example.abeete3.fragment


import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.abeete3.R
import com.example.abeete3.databinding.FragmentHomeBinding
import kotlinx.android.synthetic.main.fragment_home.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class one : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentHomeBinding.inflate(inflater, container, false)




        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        medical.setOnClickListener{Navigation.findNavController(it).navigate(R.id.two)}
        police.setOnClickListener{Navigation.findNavController(it).navigate(R.id.three)}
        fire.setOnClickListener{Navigation.findNavController(it).navigate(R.id.four)}
        firstaid.setOnClickListener{Navigation.findNavController(it).navigate(R.id.five)}
    }

}
