package com.example.abeete3.fragment

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.AsyncTask
import android.os.Build
import androidx.annotation.RequiresApi
import com.example.abeete3.ExampleService
import com.example.abeete3.MainActivity
import com.example.abeete3.R

class getBackgroundNotification(private val context: Context, private val myservice:ExampleService?):
    AsyncTask<Long, Void, Any>()
{

    private lateinit var mNotification: Notification
    private val mNotificationId:Int =1000

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun doInBackground(vararg p0: Long?): Any? {
        createChannel(context)
        var mNotification: Notification
        var notificationManager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notifyIntent = Intent(context, MainActivity::class.java)

        val title=" Abeete"
        val message = "Tap to Open your Emergency App"
        notifyIntent.putExtra("title", title)
        notifyIntent.putExtra("message", message)
        notifyIntent.putExtra("notification", true)
        notifyIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        val pendingInt = PendingIntent.getActivity(context, 0, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            mNotification = Notification.Builder(context, ExampleService.CHANNEL_ID)
                .setContentIntent(pendingInt)
                .setSmallIcon(R.drawable.notification_template_icon_low_bg)
                .setAutoCancel(true)
                .setContentTitle(title)
                .setStyle(Notification.BigTextStyle().bigText(message))
                .setContentText(message).build()
        }else
        {
            mNotification = Notification.Builder(context)
                .setContentIntent(pendingInt)
                .setSmallIcon(R.drawable.notification_template_icon_low_bg)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentTitle(title)
                .setStyle(Notification.BigTextStyle().bigText(message))
                .setContentText(message).build()
        }
        myservice?.startForeground(999, mNotification)
        return null
    }
    private fun createChannel(context: Context)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            val notificationManager=context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(ExampleService.CHANNEL_ID, ExampleService.CHANNEL_NAME, importance)
            notificationChannel.enableVibration(true)
            notificationChannel.setShowBadge(true)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.parseColor("#e8334a")
            notificationChannel.description ="Tap to get you emergency app"
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }
    companion object
    {
        const val CHANNEL_ID ="SDFGHJKL"
        const val CHANNEL_NAME ="Abeete Notification"
    }
}
