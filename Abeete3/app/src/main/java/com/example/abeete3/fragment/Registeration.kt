package com.example.abeete3.fragment


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.abeete3.R

import com.example.abeete3.data.User
import com.example.abeete3.databinding.FragmentProfileBinding
import com.example.abeete3.databinding.FragmentRegistrationBinding
import com.example.abeete3.viewmodel.UserViewModel

import java.io.ByteArrayOutputStream

import kotlinx.android.synthetic.main.fragment_registration.*



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class seven : Fragment() {

    private lateinit var listener:RegisterButtonSelected
    private lateinit var image:ByteArray
    private lateinit var add: Button
    private lateinit var userViewModel: UserViewModel
    private lateinit var binding:FragmentRegistrationBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is RegisterButtonSelected)
        {
            listener = context
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater ,R.layout.fragment_registration,container , false)

        userViewModel= ViewModelProviders.of(this).get(UserViewModel::class.java)

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bloodtype = arrayOf("O", "A", "AB", "B")
        val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, bloodtype)
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        BloodType.adapter = adapter


        binding.registerButton.setOnClickListener {
           // binding?.let {

                userViewModel.insertUser(User(
                    binding.FullName.text.toString(),
                    binding.City.text.toString(),
                    binding.HouseNo.text.toString(),
                    binding.Heigh.text.toString().toDouble(),
                    binding.Weight.text.toString().toDouble(),
                    binding.BloodType.selectedItem.toString(),
                    binding.dOB.text.toString(),
                    binding.Allergy.text.toString(),
                    binding.MedicalConditions.text.toString(),
                    binding.Medications.text.toString()

                ))

            //}
            binding.FullName.setText("")
            binding.City.setText("")
            binding.HouseNo.setText("")
            binding.Heigh.setText("")
            binding.Weight.setText("")

            binding.dOB.setText("")
            binding.Allergy.setText("")
            binding.MedicalConditions.setText("")
            binding.Medications.setText("")

            findNavController().navigate(R.id.eight, null)
           // val nextAction = sevenDirections.nextAction()
            //Navigation.findNavController(it).navigate(nextAction)
            //listener.registerButtonSelected(readFields(binding))
        }}
        private fun pickImageFromGallery() {
            //Intent to pick image
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, IMAGE_PICK_CODE)
        }
        companion object {
            //image pick code
            private val IMAGE_PICK_CODE = 1000;
            //Permission code
            private val PERMISSION_CODE = 1001;
        }
        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE){
                imageView3.setImageURI(data?.data)
            }
        }
        private fun readFields(binding: FragmentRegistrationBinding) : User {
            //val bitmap=(imageView3.drawable as BitmapDrawable).getBitmap()
            //val stream= ByteArrayOutputStream()
            //bitmap.compress(Bitmap.CompressFormat.PNG,90,stream)
            //image=stream.toByteArray()
            return User(
                binding.FullName.text.toString(),
                binding.City.text.toString(),
                binding.HouseNo.text.toString(),
                binding.Heigh.text.toString().toDouble(),
                binding.Weight.text.toString().toDouble(),
                binding.BloodType.selectedItem.toString(),
                binding.dOB.text.toString(),
                binding.Allergy.text.toString(),
                binding.MedicalConditions.text.toString(),
                binding.Medications.text.toString()

            )}


        interface RegisterButtonSelected
        {

            fun getUserByName(name:String)
            fun notNowButtonSelected()

        }


}
