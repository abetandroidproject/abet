package com.example.abeete3.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.abeete.data.EmergencyDao
import com.example.abeete.data.UserDao

@Database(entities = arrayOf(User::class,FirstAid::class, EmergencyContact::class),version=3)
abstract class AbetDatabase :RoomDatabase(){

    abstract fun userDao(): UserDao
    abstract fun firstAidDao(): FirstAidDao
    abstract fun emergencyDao(): EmergencyDao
    companion object {
        @Volatile
        private var INSTANCE: AbetDatabase? =null
        fun getDatabase(context: Context): AbetDatabase {
            val  tempInstance= INSTANCE
            if(tempInstance!=null){
                return tempInstance

            }
            synchronized(this)
            {
                val instance= Room.databaseBuilder(context.applicationContext, AbetDatabase::class.java,"abet_database").build()
                INSTANCE=instance
                return instance
            }

        }
    }
}