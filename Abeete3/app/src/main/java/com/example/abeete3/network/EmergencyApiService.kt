package com.example.abeete3.network
import com.example.abeete3.data.EmergencyContact
import com.example.abeete3.data.User
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
interface EmergencyApiService {
    @GET("contact/all")
    fun findAllAsync(): Deferred<Response<List<EmergencyContact>>>
    @GET("contact/{name}")
    fun findByNameAsync(@Path("name") name: String): Deferred<Response<EmergencyContact>>
    @POST("contact")
    fun insertContactAsync(@Body contact: EmergencyContact): Deferred<Response<Void>>
    @PUT("contact/{id}")
    fun updateContactAsnc(@Path("name") name: String, @Body contact: EmergencyContact): Deferred<Response<Void>>
    @DELETE("contact/{id}")
    fun deleteContactAsync(@Path("name") name: String): Deferred<Response<Void>>


    companion object {

        private val baseUrl = "http://192.168.127.1:7080/"

        fun getInstance():EmergencyApiService{

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit =  Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(EmergencyApiService::class.java)
        }
    }
}