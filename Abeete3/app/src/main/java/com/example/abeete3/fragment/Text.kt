package com.example.abeete3.fragment


import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.abeete3.R
import com.example.abeete3.databinding.FragmentProfileBinding
import com.example.abeete3.databinding.FragmentTextBinding
import com.example.abeete3.viewmodel.FirstAidViewModel
import kotlinx.android.synthetic.main.fragment_firstaidinfo.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class Text : Fragment() {

    private lateinit var firstAidViewModel: FirstAidViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        firstAidViewModel = ViewModelProviders.of(this).get(FirstAidViewModel::class.java)
        val binding = FragmentTextBinding.inflate(inflater, container, false)
        val searchT=search.query.toString()
        //search.setOnQueryTextListener(on)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*if(connected()) {
            val info=firstAidViewModel.allInfos.observe(this, Observer { response ->
                response.body()?.run{ {

                }
            })
        }*/
    }

    private fun connected():Boolean {

        val connectivityManager =  (activity)!!.getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }
}
