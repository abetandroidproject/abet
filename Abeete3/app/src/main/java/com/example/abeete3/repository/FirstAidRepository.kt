package com.example.abeete3.repository

import android.util.Log
import androidx.lifecycle.LiveData
import retrofit2.Response

import com.example.abeete.data.UserDao
import com.example.abeete3.data.FirstAid
import com.example.abeete3.data.FirstAidDao
import com.example.abeete3.data.User
import com.example.abeete3.network.FirstAidApiService
import com.example.abeete3.network.UserApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class FirstAidRepository(private val firstAidDao: FirstAidDao, private val firstAidApiService: FirstAidApiService) {


    fun getInfoByTitle(title:String):LiveData<FirstAid>{
        return firstAidDao.getInfo(title)
    }

    fun allInfo():LiveData<FirstAid>{
        return firstAidDao.getAllInfos()
    }
    suspend fun allInfoApi(){
        val response: Response<List<FirstAid>> = firstAidApiService.findAllAsync().await()
        val infos = response.body()


    }



    suspend fun findByTitle(title:String){
        val response: Response<FirstAid> = firstAidApiService.findByTitleAsync(title).await()
        val info = response.body()

    }


}
