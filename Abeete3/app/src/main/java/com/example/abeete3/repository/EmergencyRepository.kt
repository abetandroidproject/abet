package com.example.abeete3.repository

import android.util.Log
import androidx.lifecycle.LiveData
import com.example.abeete.data.EmergencyDao
import retrofit2.Response


import com.example.abeete3.data.EmergencyContact
import com.example.abeete3.data.User
import com.example.abeete3.network.EmergencyApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class EmergencyRepository(private val emergencyDao: EmergencyDao, private val emergencyApiService: EmergencyApiService) {

    fun insertContact(contact:EmergencyContact):Long{
        return emergencyDao.insertContact(contact)
    }
    fun getContactByName(name:String):LiveData<EmergencyContact>{
        return emergencyDao.getContact(name)
    }
    fun allContacts():LiveData<EmergencyContact>{
        return emergencyDao.allContacts()
    }
    suspend fun allContactsApi(){
        val response: Response<List<EmergencyContact>> = emergencyApiService.findAllAsync().await()
        val contacts = response.body()
        if(contacts != null){
            //Log.d("Repo all" , users[0].name)
            for(contact in contacts){
                emergencyDao.insertContact(contact)
            }
        }
    }



    suspend fun insertContactApi(contact:EmergencyContact){
        //Log.d("Repo", user.name)
        withContext(Dispatchers.IO) {
            emergencyApiService.insertContactAsync(contact).await()
        }
    }

    fun deleteContactApi(contact:EmergencyContact){
       emergencyApiService.deleteContactAsync(contact.name)
    }

    fun updateContactApi(contact:EmergencyContact){
       emergencyApiService.updateContactAsnc(contact.name,contact)
    }

    suspend fun findByName(name:String){
        val response: Response<EmergencyContact> = emergencyApiService.findByNameAsync(name).await()
        val contact = response.body()
        if(contact != null){
           emergencyDao.insertContact(contact)
        }
    }


}
