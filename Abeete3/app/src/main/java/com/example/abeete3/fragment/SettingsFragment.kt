package com.example.abeete3.fragment


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.example.abeete3.R
import com.example.abeete3.data.EmergencyContact
import com.example.abeete3.data.User
import com.example.abeete3.databinding.FragmentProfileBinding
import com.example.abeete3.databinding.FragmentRegistrationBinding
import com.example.abeete3.databinding.FragmentSettingsBinding
import com.example.abeete3.viewmodel.EmergencyViewModel
import com.example.abeete3.viewmodel.UserViewModel


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class settings_fragment : Fragment() {
    private lateinit var emergencyViewModel: EmergencyViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentSettingsBinding.inflate(inflater, container, false)
        emergencyViewModel= ViewModelProviders.of(this).get(EmergencyViewModel::class.java)

        binding.addButton.setOnClickListener {
            binding?.let {
               emergencyViewModel.insertContact(readFields(binding))
            }
        }
        binding.editButton.setOnClickListener {
            binding?.let {
                emergencyViewModel.insertContact(readFields(binding))
            }        }
        return binding.root
    }
    private fun readFields(binding: FragmentSettingsBinding) : EmergencyContact{
        //val bitmap=(imageView3.drawable as BitmapDrawable).getBitmap()
        //val stream= ByteArrayOutputStream()
        //bitmap.compress(Bitmap.CompressFormat.PNG,90,stream)
        //image=stream.toByteArray()
        return EmergencyContact(
            binding.name.text.toString(),
            binding.phone.text.toString(),
            binding.relationship.selectedItem.toString()


        )
    }


}
