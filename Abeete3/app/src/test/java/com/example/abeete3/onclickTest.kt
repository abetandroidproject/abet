package com.example.abeete3

import androidx.navigation.Navigation
import com.example.abeete3.fragment.one
import java.util.regex.Pattern.matches

class onclickTest {

    @get:Rule
    var activityRule: ActivityTestRule<Home>
            = ActivityTestRule(one::class.java)

    @Test
    fun gotoFragmentOne() {
        onView(withId(R.id.police)).perform(
            Navigation.createNavigateOnClickListener(R.id.two)
        )
        onView(withId(R.id.police))
            .check(onclickTest(Navigation.createNavigateOnClickListener(R.id.two))


    }
    @Test
    fun gotoFragmenttwo() {
        onView(withId(R.id.medical)).perform(
            Navigation.createNavigateOnClickListener(R.id.three)
        )
        onView(withId(R.id.medical))
            .check(onclickTest(Navigation.createNavigateOnClickListener(R.id.three)


    }
    }
