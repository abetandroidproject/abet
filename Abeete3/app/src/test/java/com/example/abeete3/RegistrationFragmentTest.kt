package com.example.abeete3

import android.content.Intent
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.example.abeete.data.UserDatabase
import com.example.abeete3.repository.UserRepository
import com.example.abeete3.viewmodel.UserViewModel
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith


@RunWith(AndroidJUnit::class)
@MediumTest
@LooperMode(LooperMode.Mode.PAUSED)
@TextLayoutMode(TextLayoutMode.Mode.REALISTIC)
@ExperimentalCoroutinesApi
class RegistrationFragmentTest {

    private lateinit var repo: AbetDatabase
    private lateinit var database: UserDatabase
    private lateinit var viewModel: UserViewModel


    @get:Rule
    @Rule
    @JvmField
    val activityRule = object : ActivityTestRule<MainActivity>(MainActivity::class.java) {
        override fun getActivityIntent(): Intent {
            return Intent(InstrumentationRegistry.getInstrumentation().context, MainActivity::class.java)

        }
    }


    @Rule
    fun rule() = activityRule


    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AbetDatabase::class.java,
            "abet_db").allowMainThreadQueries().build()

        repo = UserRepository(database.getInstance(), UserApiService.getInstance())
        viewModel = UserViewModel(ApplicationProvider.getApplicationContext())
        rule().activity.supportFragmentManager.beginTransaction()
    }
    @After
    fun cleanUp() {
        database.close()
    }

      @org.junit.Test
    fun displayTask_whenRepositoryHasData() =
          runBlockingTest{
              // GIVEN - One group already in the repository
              repo.insertUser(
                  User("sjhdf", "dsfjhd", "dsfjhdf", 11, 33, "A", "sdfgh", "sxdcfgh", "dfghj", "dfgh")
              val scenario = launchFragmentInContainer<ProfileFragment>()


              // THEN - Verify group is displayed on screen
              Espresso.onView(ViewMatchers.withText("sjhdf")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
          }

}