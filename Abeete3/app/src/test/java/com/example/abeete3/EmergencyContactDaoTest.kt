package com.example.abeete3

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry
import com.example.abeete3.data.EmergencyContact
import com.example.abeete3.data.EmergencyContactDao
import com.example.abeete3.data.EmergencyContactDatabase
import org.junit.After
import org.junit.Before
import org.junit.Rule
@RunWith(AndroidJUnit4::class)
@SmallTest
@ExperimentalCoroutinesApi
class EmergencyContactDaoTest {

    private lateinit var database: AbetDatabase
    private lateinit var contactDao: EmergencyDao
    private val contactA = EmergencyContact(1, "SXDFGHJ", "23456", "Mother", 7)
    private val contactB = EmergencyContact(2, "fghjj", "3456789", "Father", 9)
    private val contactC = EmergencyContact(3, "ngdtgdgff", "23456789", "Brother", 8)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, AbetDatabase::class.java).build()
        contactDao = database.emergencyDao()


       val insertedID = contactDao.insert(contactA)
        assertNotNull(insertedID)
        val insertedID1 =contactDao.insert(contactB)
        assertNotNull(insertedID1)
        val insertedID2 = contactDao.insert(contactC)
        assertNotNull(insertedID2)
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    @Throws(Exception::class)
    fun readAndWriteTests() {

        // find by id
        val inserted = contactDao!!.getContactByName()
        assertNotNull(inserted)
        assertTrue(inserted[0] == contactA.text)
        assertTrue(inserted[1] == contactB.text)
        assertTrue(inserted[2] == contactC.text)


        // update
        val updatedQtd = contactDao!!.updateContact(contactA)
        assertEquals(updatedQtd.toLong(), 1)


    }

}