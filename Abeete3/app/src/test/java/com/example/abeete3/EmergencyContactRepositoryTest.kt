package com.example.abeete3

import com.example.abeete3.repository.EmergencyContactRepository
import com.example.abeete3.data.EmergencyContactDatabase
import com.example.abeete3.network.EmergencyContactApiService

class EmergencyContactRepositoryTest {
 private lateinit var repo: EmergencyRepository
    private lateinit var database: AbetDatabase

    // Executes each task synchronously using Architecture Components.
    @get:org.junit.Rule
    var instantExecutorRule = androidx.arch.core.executor.testing.InstantTaskExecutorRule()

    @org.junit.Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = androidx.room.Room.inMemoryDatabaseBuilder(
            androidx.test.core.app.ApplicationProvider.getApplicationContext<android.content.Context>(),
            AbetDatabase::class.java).allowMainThreadQueries().build()

        repo = EmergencyContactRepository(database.getInstance().emergencyDao, EmergencyApiService.getInstance())
    }

    @org.junit.After
    fun cleanUp() {
        database.close()
    }
    @org.junit.Test
    fun insertAndRetrieve()= runBlocking{
        // GIVEN - a new group saved in the database
        val contact = EmergencyContact(1, "dfgthyj", "45678", "Mother", 11)
        repo.insertContact(contact)

        // WHEN  - Group retrieved by code
        val result  = repo.getContactByName[0](contact.name)

        org.junit.Assert.assertThat(result, org.hamcrest.CoreMatchers.notNullValue())

        // THEN - Same group is returned
        Assert.assertThat(result.value?.name, CoreMatchers.`is`("dfgthyj"))
        Assert.assertThat(result.value?.phoneNumber, CoreMatchers.`is`("45678"))
        Assert.assertThat(result.value?.id, CoreMatchers.`is`(1))

    }

}