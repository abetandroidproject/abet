package com.example.abeete3

import androidx.room.Room
import com.example.abeete.data.UserDao
import com.example.abeete.data.UserDatabase
import junit.framework.Assert.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class UserDaoTest {


    private var userDao: UserDao? = null
    private var db: UserDatabase? = null

    @Before
    fun onCreateDB() {
        val context = InstrumentationRegistry.getTargetContext()
        db = Room.databaseBuilder(context, UserDatabase::class.java, "User dao").build()
        userDao = db!!.userDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDB() {
        db!!.close()
    }

    @Test
    @Throws(Exception::class)
    fun readAndWriteTests() {
        val user = TestUtil.createNote()

        // insert
        val insertedID = userDao!!.insertUser(user)
        assertNotNull(insertedID)

        // find by id
        val inserted = userDao!!.getUser(user)
        assertNotNull(inserted)
        assertTrue(inserted == user.text)

        // update
        val updatedQtd = userDao!!.updateUser(inserted)
        assertEquals(updatedQtd.toLong(), 1)


    }
