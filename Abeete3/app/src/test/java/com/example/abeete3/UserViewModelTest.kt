package com.example.abeete3

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.example.abeete.data.UserDatabase
import com.example.abeete3.network.UserApiService
import com.example.abeete3.repository.UserRepository
import com.example.abeete3.viewmodel.UserViewModel
import org.junit.After
import org.junit.Before
import org.junit.Rule

@org.junit.runner.RunWith(AndroidJUnit4::class)
class UserViewModelTest {

    private lateinit var appDatabase: AbetDatabase
    private lateinit var viewModel: UserViewModel
    private lateinit var userApiService: UserApiService

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, AbetDatabase::class.java).build()

        userApiService = UserApiService.getInstace()
        val userRepo = UserRepository.getInstance(appDatabase.userDao(), userApiService)
        viewModel = UserViewModel(context)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }
}