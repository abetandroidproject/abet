package com.example.abeete3

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.example.abeete3.data.FirstAidDatabase
import com.example.abeete3.viewmodel.FirstAidViewModel
import org.junit.After
import org.junit.Before
import org.junit.Rule

@org.junit.runner.RunWith(AndroidJUnit4::class)
class FirstAidViewModelTest {

    private lateinit var appDatabase: AbetDatabase
    private lateinit var viewModel: FirstAidViewModel
    private lateinit var firstAidApiService:FirstAidApiService

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, AbetDatabase::class.java).build()

        firstAidApiService = FirstAidApiService.getInstance()
        val firstAidRepo = FirstAidRepository.getInstance(appDatabase.firsaidDao(), firstAidApiService)
        viewModel = FirstAidViewModel(context)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }
}