package com.example.abeete3

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.example.abeete3.data.EmergencyContactDatabase
import org.junit.After
import org.junit.Before
import org.junit.Rule
@org.junit.runner.RunWith(AndroidJUnit4::class)
class EmergencyContactViewModelTest {

    private lateinit var appDatabase: AbetDatabase
    private lateinit var viewModel: EmergencyViewModel
    private lateinit var emergencyContactApiService:EmergencyApiService

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context, AbetDatabase::class.java).build()

        emergencyContactApiService = EmergencyApiService.getInstance()
        val contactRepo = EmergencyContactRepository.getInstance(appDatabase.emergencyDao(), emergencyContactApiService)
        viewModel = EmergencyContactViewModel(context)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }
}