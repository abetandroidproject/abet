package com.example.abeete3

import android.content.Intent
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.example.abeete3.data.EmergencyContact
import org.junit.After
import com.example.abeete3.data.EmergencyContactDatabase
import com.example.abeete3.repository.EmergencyContactRepository
import com.example.abeete3.network.EmergencyContactApiService
import com.example.abeete3.viewmodel.EmrgencyContactViewModel
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit::class)
@MediumTest
@LooperMode(LooperMode.Mode.PAUSED)
@TextLayoutMode(TextLayoutMode.Mode.REALISTIC)
@ExperimentalCoroutinesApi
class SettingFragmentTest {

    private lateinit var repo: EmergencyRepository
    private lateinit var database: AbetDatabase
    private lateinit var viewModel: EmrgencyViewMode

    @get:Rule
    @Rule
    @JvmField
    val activityRule = object : ActivityTestRule<MainActivity>(MainActivity::class.java) {
        override fun getActivityIntent(): Intent {
            return Intent(InstrumentationRegistry.getInstrumentation().context, MainActivity::class.java)

        }
    }

    @Rule
    fun rule() = activityRule

    @Before
    fun setup() {
        // using an in-memory database for testing, since it doesn't survive killing the process
        database = Room.databaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AbetDatabase::class.java,
            "abet_db").allowMainThreadQueries().build()

        repo = EmergencyContactRepository(database.getInstance(), EmergencyContactApiService.getInstance())
        viewModel = EmergencyContactViewModel(ApplicationProvider.getApplicationContext())
        rule().activity.supportFragmentManager.beginTransaction()
    }
    @After
    fun cleanUp() {
        database.close()
    }

    @After
    fun cleanUp() {
        database.close()
    }

    @org.junit.Test
    fun displayTask_whenRepositoryHasData() =
        runBlockingTest{
            // GIVEN - One group already in the repository
            repo.insertContact(
                EmergencyContact( 1, "sdjdf", "12434234", "Mother", 5)
                )
            val scenario = launchFragmentInContainer<SettingFragment>()


            // THEN - Verify group is displayed on screen
            Espresso.onView(ViewMatchers.withText("sdjdf")).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        }



}