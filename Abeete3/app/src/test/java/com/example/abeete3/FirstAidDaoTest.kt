package com.example.abeete3

import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.example.abeete.data.UserDao
import com.example.abeete.data.UserDatabase
import com.example.abeete3.data.AbetDatabase
import com.example.abeete3.data.FirstAidDao
import com.example.abeete3.data.FirstAidDatabase
import junit.framework.Assert
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

//@RunWith(AndroidJUnit4::class)
class FirstAidDaoTest {


    private var firstAidDao: FirstAidDao? = null
    private var db: AbetDatabase? = null

    @Before
    fun onCreateDB() {
        val context = InstrumentationRegistry.getTargetContext()
        db = Room.databaseBuilder(context, FirstAidDatabase::class.java, "User dao").build()
        firstAidDao = db!!.firstAidInfo()
    }

    @After
    @Throws(IOException::class)
    fun closeDB() {
        db!!.close()
    }

    @Test
    @Throws(Exception::class)
    fun readAndWriteTests() {
       // val user = TestUtil.createNote()

        // insert
        val insertedID = firstAidDao!!.getAllInfos()
        Assert.assertNotNull(insertedID)

        // find by id
        //val inserted = firstAidDao!!.getInfo("title")
        /*Assert.assertNotNull(inserted)
        Assert.assertTrue(inserted == user.text)

        //find all
        val insertedlist = firstAidDao!!.getAllInfos()
        Assert.assertNotNull(inserted)
        Assert.assertTrue(inserted == user.text)*/

    }
}