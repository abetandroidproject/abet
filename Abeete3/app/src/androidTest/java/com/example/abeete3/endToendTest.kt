package com.example.abeete3

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.example.abeete.data.UserDao
import com.example.abeete3.data.EmergencyContactDatabase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.Rule

@org.junit.runner.RunWith(AndroidJUnit::class)
@LargeTest
class endToendTest {

    private lateinit var userViewModel: UserViewModel

    private lateinit var database: UserDatabase

    private lateinit var userRepository: UserRepository

    private lateinit var result : User

    private lateinit var userDao:UserDao

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    // Executes each task synchronously using Architecture Components.
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    var user = User("jdgdf", "dfgjf", "dfjhgf", 32, 22, "dfkjjf", "dfgdfg", "fdgjkhf", "dfgjh")

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, AbetDatabase::class.java).build()
        userDao = database.userDao()


        val insertedID = contactDao.insert(user)
        assertNotNull(insertedID)
    }


    @Before
    fun setupViewModel() {
        database=
            Room.databaseBuilder(
                ApplicationProvider.getApplicationContext(),
                AbetDatabase::class.java,
                "user_db").allowMainThreadQueries().build()

        // We initialise the repository with no tasks
        userRepository = UserRepository(database.getInstance().userDao(), UserApiService.getInstance())

        // Create class under test
        userViewModel = UserViewModel(ApplicationProvider.getApplicationContext())


    }

    @After
    fun closeDb() = database.close()

    @org.junit.Test
    fun registrationTest()
    {
        val scenario = launchFragmentInContainer<RegistrationFragment>(Bundle(), R.style.AppTheme)
        val navController = Mockito.mock(NavController::class.java)
        scenario.onFragment {
            Navigation.setViewNavController(it.view!!, navController)

            onView(withId(R.id.registerButton)).perform(click())
        }
    }

}