package com.example.abeete3.network;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0005\bf\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012J\u001e\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0007H\'J\u001a\u0010\b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u00040\u0003H\'J\u001e\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u0007H\'J\u001e\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u00040\u00032\b\b\u0001\u0010\r\u001a\u00020\u000eH\'J\u001e\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0010\u001a\u00020\nH\'J(\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\u0010\u001a\u00020\nH\'\u00a8\u0006\u0013"}, d2 = {"Lcom/example/abeete3/network/FirstAidApiService;", "", "deleteInfoAsync", "Lkotlinx/coroutines/Deferred;", "Lretrofit2/Response;", "Ljava/lang/Void;", "id", "", "findAllAsync", "", "Lcom/example/abeete3/data/FirstAid;", "findByIdAsync", "findByTitleAsync", "title", "", "insertInfoAsync", "newInfo", "updateInfoAsnc", "Companion", "app_debug"})
public abstract interface FirstAidApiService {
    public static final com.example.abeete3.network.FirstAidApiService.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "info/all")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<java.util.List<com.example.abeete3.data.FirstAid>>> findAllAsync();
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "info/{id}")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<com.example.abeete3.data.FirstAid>> findByIdAsync(@retrofit2.http.Path(value = "id")
    long id);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "info")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<com.example.abeete3.data.FirstAid>> findByTitleAsync(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "title")
    java.lang.String title);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "info")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<java.lang.Void>> insertInfoAsync(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.example.abeete3.data.FirstAid newInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.PUT(value = "info/{id}")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<java.lang.Void>> updateInfoAsnc(@retrofit2.http.Path(value = "id")
    long id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.example.abeete3.data.FirstAid newInfo);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.DELETE(value = "info/{id}")
    public abstract kotlinx.coroutines.Deferred<retrofit2.Response<java.lang.Void>> deleteInfoAsync(@retrofit2.http.Path(value = "id")
    long id);
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/example/abeete3/network/FirstAidApiService$Companion;", "", "()V", "baseUrl", "", "getInstance", "Lcom/example/abeete3/network/FirstAidApiService;", "app_debug"})
    public static final class Companion {
        private static final java.lang.String baseUrl = "http://192.168.127.1:7080/";
        
        @org.jetbrains.annotations.NotNull()
        public final com.example.abeete3.network.FirstAidApiService getInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}