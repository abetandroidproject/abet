package com.example.abeete3;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0016\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\u000eJ\b\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u001aH\u0016J\u0010\u0010\u001d\u001a\u00020\u000e2\u0006\u0010\u001c\u001a\u00020\u001aH\u0016R*\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR*\u0010\r\u001a\u0012\u0012\u0004\u0012\u00020\u000e0\u0006j\b\u0012\u0004\u0012\u00020\u000e`\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\n\"\u0004\b\u0010\u0010\fR\u001a\u0010\u0011\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0004\u00a8\u0006\u001e"}, d2 = {"Lcom/example/abeete3/FirstAidAdapter;", "Landroidx/fragment/app/FragmentStatePagerAdapter;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "(Landroidx/fragment/app/FragmentManager;)V", "mFragmentItem", "Ljava/util/ArrayList;", "Landroidx/fragment/app/Fragment;", "Lkotlin/collections/ArrayList;", "getMFragmentItem", "()Ljava/util/ArrayList;", "setMFragmentItem", "(Ljava/util/ArrayList;)V", "mFragmentTitles", "", "getMFragmentTitles", "setMFragmentTitles", "mfm", "getMfm", "()Landroidx/fragment/app/FragmentManager;", "setMfm", "addFragment", "", "fragmentItem", "fragmentTitle", "getCount", "", "getItem", "position", "getPageTitle", "app_debug"})
public final class FirstAidAdapter extends androidx.fragment.app.FragmentStatePagerAdapter {
    @org.jetbrains.annotations.NotNull()
    private androidx.fragment.app.FragmentManager mfm;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<androidx.fragment.app.Fragment> mFragmentItem;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<java.lang.String> mFragmentTitles;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.fragment.app.FragmentManager getMfm() {
        return null;
    }
    
    public final void setMfm(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentManager p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<androidx.fragment.app.Fragment> getMFragmentItem() {
        return null;
    }
    
    public final void setMFragmentItem(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<androidx.fragment.app.Fragment> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.String> getMFragmentTitles() {
        return null;
    }
    
    public final void setMFragmentTitles(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> p0) {
    }
    
    public final void addFragment(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment fragmentItem, @org.jetbrains.annotations.NotNull()
    java.lang.String fragmentTitle) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.fragment.app.Fragment getItem(int position) {
        return null;
    }
    
    @java.lang.Override()
    public int getCount() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String getPageTitle(int position) {
        return null;
    }
    
    public FirstAidAdapter(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentManager fragmentManager) {
        super(null);
    }
}