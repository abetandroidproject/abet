package com.example.abeete3.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bJ\u0011\u0010\n\u001a\u00020\u000bH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\fJ\u000e\u0010\r\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\tJ\u0019\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u0011H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0012J\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\u0010\u001a\u00020\u0011J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u000e\u001a\u00020\tJ\u0019\u0010\u0016\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\tH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0017J\u000e\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\tR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0019"}, d2 = {"Lcom/example/abeete3/repository/EmergencyRepository;", "", "emergencyDao", "Lcom/example/abeete/data/EmergencyDao;", "emergencyApiService", "Lcom/example/abeete3/network/EmergencyApiService;", "(Lcom/example/abeete/data/EmergencyDao;Lcom/example/abeete3/network/EmergencyApiService;)V", "allContacts", "Landroidx/lifecycle/LiveData;", "Lcom/example/abeete3/data/EmergencyContact;", "allContactsApi", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "deleteContactApi", "contact", "findByName", "name", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getContactByName", "insertContact", "", "insertContactApi", "(Lcom/example/abeete3/data/EmergencyContact;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "updateContactApi", "app_debug"})
public final class EmergencyRepository {
    private final com.example.abeete.data.EmergencyDao emergencyDao = null;
    private final com.example.abeete3.network.EmergencyApiService emergencyApiService = null;
    
    public final long insertContact(@org.jetbrains.annotations.NotNull()
    com.example.abeete3.data.EmergencyContact contact) {
        return 0L;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.abeete3.data.EmergencyContact> getContactByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.abeete3.data.EmergencyContact> allContacts() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object allContactsApi(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p0) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object insertContactApi(@org.jetbrains.annotations.NotNull()
    com.example.abeete3.data.EmergencyContact contact, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    public final void deleteContactApi(@org.jetbrains.annotations.NotNull()
    com.example.abeete3.data.EmergencyContact contact) {
    }
    
    public final void updateContactApi(@org.jetbrains.annotations.NotNull()
    com.example.abeete3.data.EmergencyContact contact) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object findByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> p1) {
        return null;
    }
    
    public EmergencyRepository(@org.jetbrains.annotations.NotNull()
    com.example.abeete.data.EmergencyDao emergencyDao, @org.jetbrains.annotations.NotNull()
    com.example.abeete3.network.EmergencyApiService emergencyApiService) {
        super();
    }
}