package com.example.abeete3.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0007J\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\u0018\u001a\u00020\u000eJ\u000e\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0007J\u000e\u0010\u001a\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0007R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lcom/example/abeete3/viewmodel/EmergencyViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "allContacts", "Landroidx/lifecycle/LiveData;", "Lcom/example/abeete3/data/EmergencyContact;", "getAllContacts", "()Landroidx/lifecycle/LiveData;", "emergencyRepository", "Lcom/example/abeete3/repository/EmergencyRepository;", "filterData", "Landroidx/lifecycle/MutableLiveData;", "", "searchData", "connected", "", "context", "Landroid/content/Context;", "deleteUser", "Lkotlinx/coroutines/Job;", "contact", "getContactByName", "name", "insertContact", "updateUser", "app_debug"})
public final class EmergencyViewModel extends androidx.lifecycle.AndroidViewModel {
    private final com.example.abeete3.repository.EmergencyRepository emergencyRepository = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<com.example.abeete3.data.EmergencyContact> allContacts = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.String> filterData = null;
    private androidx.lifecycle.LiveData<com.example.abeete3.data.EmergencyContact> searchData;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.abeete3.data.EmergencyContact> getAllContacts() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job insertContact(@org.jetbrains.annotations.NotNull()
    com.example.abeete3.data.EmergencyContact contact) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job deleteUser(@org.jetbrains.annotations.NotNull()
    com.example.abeete3.data.EmergencyContact contact) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.Job updateUser(@org.jetbrains.annotations.NotNull()
    com.example.abeete3.data.EmergencyContact contact) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.abeete3.data.EmergencyContact> getContactByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name) {
        return null;
    }
    
    private final boolean connected(android.content.Context context) {
        return false;
    }
    
    public EmergencyViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}