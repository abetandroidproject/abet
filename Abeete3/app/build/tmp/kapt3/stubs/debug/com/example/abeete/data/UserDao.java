package com.example.abeete.data;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\'J\u0016\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0006\u001a\u00020\u0007H\'J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0004H\'\u00a8\u0006\u000b"}, d2 = {"Lcom/example/abeete/data/UserDao;", "", "AllUsers", "Landroidx/lifecycle/LiveData;", "Lcom/example/abeete3/data/User;", "getUserByName", "name", "", "insertUser", "", "user", "app_debug"})
public abstract interface UserDao {
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract long insertUser(@org.jetbrains.annotations.NotNull()
    com.example.abeete3.data.User user);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM user ORDER BY name")
    public abstract androidx.lifecycle.LiveData<com.example.abeete3.data.User> AllUsers();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "Select * from user where name=:name")
    public abstract androidx.lifecycle.LiveData<com.example.abeete3.data.User> getUserByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name);
}