package com.example.abeete3;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0014\u0010!\u001a\u00020\u00072\n\u0010\"\u001a\u0006\u0012\u0002\b\u00030#H\u0002J\b\u0010$\u001a\u00020\u001eH\u0016J\u0012\u0010%\u001a\u00020\u001e2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0014J\b\u0010(\u001a\u00020\u001eH\u0014J\u0010\u0010)\u001a\u00020\u001e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0006\u0010*\u001a\u00020\u001eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\b\"\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u00020\u0018X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001c\u00a8\u0006+"}, d2 = {"Lcom/example/abeete3/MainActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/example/abeete3/fragment/seven$RegisterButtonSelected;", "()V", "appBarConfiguration", "Landroidx/navigation/ui/AppBarConfiguration;", "isBound", "", "()Z", "setBound", "(Z)V", "myConnection", "Landroid/content/ServiceConnection;", "myService", "Lcom/example/abeete3/ExampleService;", "getMyService", "()Lcom/example/abeete3/ExampleService;", "setMyService", "(Lcom/example/abeete3/ExampleService;)V", "navController", "Landroidx/navigation/NavController;", "textMessage", "Landroid/widget/TextView;", "userViewModel", "Lcom/example/abeete3/viewmodel/UserViewModel;", "getUserViewModel", "()Lcom/example/abeete3/viewmodel/UserViewModel;", "setUserViewModel", "(Lcom/example/abeete3/viewmodel/UserViewModel;)V", "getUserByName", "", "name", "", "isServiceRunning", "serviceClass", "Ljava/lang/Class;", "notNowButtonSelected", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "setupBottomNavMenu", "showTime", "app_debug"})
public final class MainActivity extends androidx.appcompat.app.AppCompatActivity implements com.example.abeete3.fragment.seven.RegisterButtonSelected {
    @org.jetbrains.annotations.NotNull()
    public com.example.abeete3.viewmodel.UserViewModel userViewModel;
    private android.widget.TextView textMessage;
    private androidx.navigation.ui.AppBarConfiguration appBarConfiguration;
    private androidx.navigation.NavController navController;
    @org.jetbrains.annotations.Nullable()
    private com.example.abeete3.ExampleService myService;
    private boolean isBound;
    private final android.content.ServiceConnection myConnection = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.abeete3.viewmodel.UserViewModel getUserViewModel() {
        return null;
    }
    
    public final void setUserViewModel(@org.jetbrains.annotations.NotNull()
    com.example.abeete3.viewmodel.UserViewModel p0) {
    }
    
    @java.lang.Override()
    public void getUserByName(@org.jetbrains.annotations.NotNull()
    java.lang.String name) {
    }
    
    @java.lang.Override()
    public void notNowButtonSelected() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.abeete3.ExampleService getMyService() {
        return null;
    }
    
    public final void setMyService(@org.jetbrains.annotations.Nullable()
    com.example.abeete3.ExampleService p0) {
    }
    
    public final boolean isBound() {
        return false;
    }
    
    public final void setBound(boolean p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setupBottomNavMenu(androidx.navigation.NavController navController) {
    }
    
    public final void showTime() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    private final boolean isServiceRunning(java.lang.Class<?> serviceClass) {
        return false;
    }
    
    public MainActivity() {
        super();
    }
}