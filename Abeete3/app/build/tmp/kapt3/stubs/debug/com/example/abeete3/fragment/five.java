package com.example.abeete3.fragment;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J&\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016J\u001a\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u00182\b\u0010\u001d\u001a\u0004\u0018\u00010\u001eH\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u00020\fX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\u0012X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016\u00a8\u0006\""}, d2 = {"Lcom/example/abeete3/fragment/five;", "Landroidx/fragment/app/Fragment;", "()V", "Tabs", "Lcom/google/android/material/tabs/TabLayout;", "getTabs", "()Lcom/google/android/material/tabs/TabLayout;", "setTabs", "(Lcom/google/android/material/tabs/TabLayout;)V", "firstAidViewModel", "Lcom/example/abeete3/viewmodel/FirstAidViewModel;", "pagerAdapter", "Lcom/example/abeete3/FirstAidAdapter;", "getPagerAdapter", "()Lcom/example/abeete3/FirstAidAdapter;", "setPagerAdapter", "(Lcom/example/abeete3/FirstAidAdapter;)V", "viewPager", "Landroidx/viewpager/widget/ViewPager;", "getViewPager", "()Landroidx/viewpager/widget/ViewPager;", "setViewPager", "(Landroidx/viewpager/widget/ViewPager;)V", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "", "view", "app_debug"})
public final class five extends androidx.fragment.app.Fragment {
    @org.jetbrains.annotations.NotNull()
    public com.google.android.material.tabs.TabLayout Tabs;
    @org.jetbrains.annotations.NotNull()
    public androidx.viewpager.widget.ViewPager viewPager;
    @org.jetbrains.annotations.NotNull()
    public com.example.abeete3.FirstAidAdapter pagerAdapter;
    private com.example.abeete3.viewmodel.FirstAidViewModel firstAidViewModel;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.tabs.TabLayout getTabs() {
        return null;
    }
    
    public final void setTabs(@org.jetbrains.annotations.NotNull()
    com.google.android.material.tabs.TabLayout p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.viewpager.widget.ViewPager getViewPager() {
        return null;
    }
    
    public final void setViewPager(@org.jetbrains.annotations.NotNull()
    androidx.viewpager.widget.ViewPager p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.abeete3.FirstAidAdapter getPagerAdapter() {
        return null;
    }
    
    public final void setPagerAdapter(@org.jetbrains.annotations.NotNull()
    com.example.abeete3.FirstAidAdapter p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public five() {
        super();
    }
}