package com.example.abeete3.fragment

import androidx.navigation.ActionOnlyNavDirections
import androidx.navigation.NavDirections
import com.example.abeete3.R

class sixDirections private constructor() {
    companion object {
        fun nextAction(): NavDirections = ActionOnlyNavDirections(R.id.next_action)
    }
}
