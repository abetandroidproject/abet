package com.example.abeete3;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.example.abeete3.databinding.ActivityMainBindingImpl;
import com.example.abeete3.databinding.FragmentContactsBindingImpl;
import com.example.abeete3.databinding.FragmentFiremapBindingImpl;
import com.example.abeete3.databinding.FragmentFirstaidinfoBindingImpl;
import com.example.abeete3.databinding.FragmentHomeBindingImpl;
import com.example.abeete3.databinding.FragmentImageBindingImpl;
import com.example.abeete3.databinding.FragmentMedicalmapBindingImpl;
import com.example.abeete3.databinding.FragmentPolicemapBindingImpl;
import com.example.abeete3.databinding.FragmentProfileBindingImpl;
import com.example.abeete3.databinding.FragmentRegistrationBindingImpl;
import com.example.abeete3.databinding.FragmentSettingsBindingImpl;
import com.example.abeete3.databinding.FragmentTextBindingImpl;
import com.example.abeete3.databinding.FragmentVideoBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYMAIN = 1;

  private static final int LAYOUT_FRAGMENTCONTACTS = 2;

  private static final int LAYOUT_FRAGMENTFIREMAP = 3;

  private static final int LAYOUT_FRAGMENTFIRSTAIDINFO = 4;

  private static final int LAYOUT_FRAGMENTHOME = 5;

  private static final int LAYOUT_FRAGMENTIMAGE = 6;

  private static final int LAYOUT_FRAGMENTMEDICALMAP = 7;

  private static final int LAYOUT_FRAGMENTPOLICEMAP = 8;

  private static final int LAYOUT_FRAGMENTPROFILE = 9;

  private static final int LAYOUT_FRAGMENTREGISTRATION = 10;

  private static final int LAYOUT_FRAGMENTSETTINGS = 11;

  private static final int LAYOUT_FRAGMENTTEXT = 12;

  private static final int LAYOUT_FRAGMENTVIDEO = 13;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(13);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_contacts, LAYOUT_FRAGMENTCONTACTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_firemap, LAYOUT_FRAGMENTFIREMAP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_firstaidinfo, LAYOUT_FRAGMENTFIRSTAIDINFO);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_home, LAYOUT_FRAGMENTHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_image, LAYOUT_FRAGMENTIMAGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_medicalmap, LAYOUT_FRAGMENTMEDICALMAP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_policemap, LAYOUT_FRAGMENTPOLICEMAP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_profile, LAYOUT_FRAGMENTPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_registration, LAYOUT_FRAGMENTREGISTRATION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_settings, LAYOUT_FRAGMENTSETTINGS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_text, LAYOUT_FRAGMENTTEXT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.example.abeete3.R.layout.fragment_video, LAYOUT_FRAGMENTVIDEO);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYMAIN: {
          if ("layout/activity_main_0".equals(tag)) {
            return new ActivityMainBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTCONTACTS: {
          if ("layout/fragment_contacts_0".equals(tag)) {
            return new FragmentContactsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_contacts is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTFIREMAP: {
          if ("layout/fragment_firemap_0".equals(tag)) {
            return new FragmentFiremapBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_firemap is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTFIRSTAIDINFO: {
          if ("layout/fragment_firstaidinfo_0".equals(tag)) {
            return new FragmentFirstaidinfoBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_firstaidinfo is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTHOME: {
          if ("layout/fragment_home_0".equals(tag)) {
            return new FragmentHomeBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTIMAGE: {
          if ("layout/fragment_image_0".equals(tag)) {
            return new FragmentImageBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_image is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTMEDICALMAP: {
          if ("layout/fragment_medicalmap_0".equals(tag)) {
            return new FragmentMedicalmapBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_medicalmap is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPOLICEMAP: {
          if ("layout/fragment_policemap_0".equals(tag)) {
            return new FragmentPolicemapBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_policemap is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTPROFILE: {
          if ("layout/fragment_profile_0".equals(tag)) {
            return new FragmentProfileBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_profile is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTREGISTRATION: {
          if ("layout/fragment_registration_0".equals(tag)) {
            return new FragmentRegistrationBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_registration is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTSETTINGS: {
          if ("layout/fragment_settings_0".equals(tag)) {
            return new FragmentSettingsBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_settings is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTTEXT: {
          if ("layout/fragment_text_0".equals(tag)) {
            return new FragmentTextBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_text is invalid. Received: " + tag);
        }
        case  LAYOUT_FRAGMENTVIDEO: {
          if ("layout/fragment_video_0".equals(tag)) {
            return new FragmentVideoBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for fragment_video is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(4);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "fromApi");
      sKeys.put(2, "toProfile");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(13);

    static {
      sKeys.put("layout/activity_main_0", com.example.abeete3.R.layout.activity_main);
      sKeys.put("layout/fragment_contacts_0", com.example.abeete3.R.layout.fragment_contacts);
      sKeys.put("layout/fragment_firemap_0", com.example.abeete3.R.layout.fragment_firemap);
      sKeys.put("layout/fragment_firstaidinfo_0", com.example.abeete3.R.layout.fragment_firstaidinfo);
      sKeys.put("layout/fragment_home_0", com.example.abeete3.R.layout.fragment_home);
      sKeys.put("layout/fragment_image_0", com.example.abeete3.R.layout.fragment_image);
      sKeys.put("layout/fragment_medicalmap_0", com.example.abeete3.R.layout.fragment_medicalmap);
      sKeys.put("layout/fragment_policemap_0", com.example.abeete3.R.layout.fragment_policemap);
      sKeys.put("layout/fragment_profile_0", com.example.abeete3.R.layout.fragment_profile);
      sKeys.put("layout/fragment_registration_0", com.example.abeete3.R.layout.fragment_registration);
      sKeys.put("layout/fragment_settings_0", com.example.abeete3.R.layout.fragment_settings);
      sKeys.put("layout/fragment_text_0", com.example.abeete3.R.layout.fragment_text);
      sKeys.put("layout/fragment_video_0", com.example.abeete3.R.layout.fragment_video);
    }
  }
}
