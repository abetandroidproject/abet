package com.example.abeete3.data;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class FirstAidDao_Impl implements FirstAidDao {
  private final RoomDatabase __db;

  public FirstAidDao_Impl(RoomDatabase __db) {
    this.__db = __db;
  }

  @Override
  public LiveData<FirstAid> getAllInfos() {
    final String _sql = "SELECT * FROM FirstAid ORDER BY title";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"FirstAid"}, false, new Callable<FirstAid>() {
      @Override
      public FirstAid call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "title");
          final int _cursorIndexOfMedication = CursorUtil.getColumnIndexOrThrow(_cursor, "medication");
          final int _cursorIndexOfSympthoms = CursorUtil.getColumnIndexOrThrow(_cursor, "sympthoms");
          final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
          final int _cursorIndexOfVideo = CursorUtil.getColumnIndexOrThrow(_cursor, "video");
          final FirstAid _result;
          if(_cursor.moveToFirst()) {
            final long _tmpId;
            _tmpId = _cursor.getLong(_cursorIndexOfId);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            final String _tmpMedication;
            _tmpMedication = _cursor.getString(_cursorIndexOfMedication);
            final String _tmpSympthoms;
            _tmpSympthoms = _cursor.getString(_cursorIndexOfSympthoms);
            final String _tmpImage;
            _tmpImage = _cursor.getString(_cursorIndexOfImage);
            final String _tmpVideo;
            _tmpVideo = _cursor.getString(_cursorIndexOfVideo);
            _result = new FirstAid(_tmpId,_tmpTitle,_tmpMedication,_tmpSympthoms,_tmpImage,_tmpVideo);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<FirstAid> getInfo(final String title) {
    final String _sql = "SELECT * FROM FirstAid WHERE title=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (title == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, title);
    }
    return __db.getInvalidationTracker().createLiveData(new String[]{"FirstAid"}, false, new Callable<FirstAid>() {
      @Override
      public FirstAid call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfTitle = CursorUtil.getColumnIndexOrThrow(_cursor, "title");
          final int _cursorIndexOfMedication = CursorUtil.getColumnIndexOrThrow(_cursor, "medication");
          final int _cursorIndexOfSympthoms = CursorUtil.getColumnIndexOrThrow(_cursor, "sympthoms");
          final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
          final int _cursorIndexOfVideo = CursorUtil.getColumnIndexOrThrow(_cursor, "video");
          final FirstAid _result;
          if(_cursor.moveToFirst()) {
            final long _tmpId;
            _tmpId = _cursor.getLong(_cursorIndexOfId);
            final String _tmpTitle;
            _tmpTitle = _cursor.getString(_cursorIndexOfTitle);
            final String _tmpMedication;
            _tmpMedication = _cursor.getString(_cursorIndexOfMedication);
            final String _tmpSympthoms;
            _tmpSympthoms = _cursor.getString(_cursorIndexOfSympthoms);
            final String _tmpImage;
            _tmpImage = _cursor.getString(_cursorIndexOfImage);
            final String _tmpVideo;
            _tmpVideo = _cursor.getString(_cursorIndexOfVideo);
            _result = new FirstAid(_tmpId,_tmpTitle,_tmpMedication,_tmpSympthoms,_tmpImage,_tmpVideo);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }
}
