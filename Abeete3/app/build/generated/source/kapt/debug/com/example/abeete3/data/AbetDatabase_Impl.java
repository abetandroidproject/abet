package com.example.abeete3.data;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import com.example.abeete.data.EmergencyDao;
import com.example.abeete.data.EmergencyDao_Impl;
import com.example.abeete.data.UserDao;
import com.example.abeete.data.UserDao_Impl;
import java.lang.IllegalStateException;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class AbetDatabase_Impl extends AbetDatabase {
  private volatile UserDao _userDao;

  private volatile FirstAidDao _firstAidDao;

  private volatile EmergencyDao _emergencyDao;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(3) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `user` (`name` TEXT NOT NULL, `city` TEXT NOT NULL, `houseno` TEXT NOT NULL, `height` REAL NOT NULL, `weight` REAL NOT NULL, `bloodType` TEXT NOT NULL, `dob` TEXT NOT NULL, `allergies` TEXT NOT NULL, `medicalConditions` TEXT NOT NULL, `medications` TEXT NOT NULL, PRIMARY KEY(`name`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `FirstAid` (`id` INTEGER NOT NULL, `title` TEXT NOT NULL, `medication` TEXT NOT NULL, `sympthoms` TEXT NOT NULL, `image` TEXT NOT NULL, `video` TEXT NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `EmergencyContact` (`name` TEXT NOT NULL, `phoneNumber` TEXT NOT NULL, `relationShip` TEXT NOT NULL, PRIMARY KEY(`name`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, \"4aa26202bcb46ccd805462b5b0503a04\")");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `user`");
        _db.execSQL("DROP TABLE IF EXISTS `FirstAid`");
        _db.execSQL("DROP TABLE IF EXISTS `EmergencyContact`");
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected void validateMigration(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsUser = new HashMap<String, TableInfo.Column>(10);
        _columnsUser.put("name", new TableInfo.Column("name", "TEXT", true, 1));
        _columnsUser.put("city", new TableInfo.Column("city", "TEXT", true, 0));
        _columnsUser.put("houseno", new TableInfo.Column("houseno", "TEXT", true, 0));
        _columnsUser.put("height", new TableInfo.Column("height", "REAL", true, 0));
        _columnsUser.put("weight", new TableInfo.Column("weight", "REAL", true, 0));
        _columnsUser.put("bloodType", new TableInfo.Column("bloodType", "TEXT", true, 0));
        _columnsUser.put("dob", new TableInfo.Column("dob", "TEXT", true, 0));
        _columnsUser.put("allergies", new TableInfo.Column("allergies", "TEXT", true, 0));
        _columnsUser.put("medicalConditions", new TableInfo.Column("medicalConditions", "TEXT", true, 0));
        _columnsUser.put("medications", new TableInfo.Column("medications", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysUser = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesUser = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoUser = new TableInfo("user", _columnsUser, _foreignKeysUser, _indicesUser);
        final TableInfo _existingUser = TableInfo.read(_db, "user");
        if (! _infoUser.equals(_existingUser)) {
          throw new IllegalStateException("Migration didn't properly handle user(com.example.abeete3.data.User).\n"
                  + " Expected:\n" + _infoUser + "\n"
                  + " Found:\n" + _existingUser);
        }
        final HashMap<String, TableInfo.Column> _columnsFirstAid = new HashMap<String, TableInfo.Column>(6);
        _columnsFirstAid.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
        _columnsFirstAid.put("title", new TableInfo.Column("title", "TEXT", true, 0));
        _columnsFirstAid.put("medication", new TableInfo.Column("medication", "TEXT", true, 0));
        _columnsFirstAid.put("sympthoms", new TableInfo.Column("sympthoms", "TEXT", true, 0));
        _columnsFirstAid.put("image", new TableInfo.Column("image", "TEXT", true, 0));
        _columnsFirstAid.put("video", new TableInfo.Column("video", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysFirstAid = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesFirstAid = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoFirstAid = new TableInfo("FirstAid", _columnsFirstAid, _foreignKeysFirstAid, _indicesFirstAid);
        final TableInfo _existingFirstAid = TableInfo.read(_db, "FirstAid");
        if (! _infoFirstAid.equals(_existingFirstAid)) {
          throw new IllegalStateException("Migration didn't properly handle FirstAid(com.example.abeete3.data.FirstAid).\n"
                  + " Expected:\n" + _infoFirstAid + "\n"
                  + " Found:\n" + _existingFirstAid);
        }
        final HashMap<String, TableInfo.Column> _columnsEmergencyContact = new HashMap<String, TableInfo.Column>(3);
        _columnsEmergencyContact.put("name", new TableInfo.Column("name", "TEXT", true, 1));
        _columnsEmergencyContact.put("phoneNumber", new TableInfo.Column("phoneNumber", "TEXT", true, 0));
        _columnsEmergencyContact.put("relationShip", new TableInfo.Column("relationShip", "TEXT", true, 0));
        final HashSet<TableInfo.ForeignKey> _foreignKeysEmergencyContact = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesEmergencyContact = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoEmergencyContact = new TableInfo("EmergencyContact", _columnsEmergencyContact, _foreignKeysEmergencyContact, _indicesEmergencyContact);
        final TableInfo _existingEmergencyContact = TableInfo.read(_db, "EmergencyContact");
        if (! _infoEmergencyContact.equals(_existingEmergencyContact)) {
          throw new IllegalStateException("Migration didn't properly handle EmergencyContact(com.example.abeete3.data.EmergencyContact).\n"
                  + " Expected:\n" + _infoEmergencyContact + "\n"
                  + " Found:\n" + _existingEmergencyContact);
        }
      }
    }, "4aa26202bcb46ccd805462b5b0503a04", "76bef4eea41204445bbc10ed2703b348");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "user","FirstAid","EmergencyContact");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `user`");
      _db.execSQL("DELETE FROM `FirstAid`");
      _db.execSQL("DELETE FROM `EmergencyContact`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public UserDao userDao() {
    if (_userDao != null) {
      return _userDao;
    } else {
      synchronized(this) {
        if(_userDao == null) {
          _userDao = new UserDao_Impl(this);
        }
        return _userDao;
      }
    }
  }

  @Override
  public FirstAidDao firstAidDao() {
    if (_firstAidDao != null) {
      return _firstAidDao;
    } else {
      synchronized(this) {
        if(_firstAidDao == null) {
          _firstAidDao = new FirstAidDao_Impl(this);
        }
        return _firstAidDao;
      }
    }
  }

  @Override
  public EmergencyDao emergencyDao() {
    if (_emergencyDao != null) {
      return _emergencyDao;
    } else {
      synchronized(this) {
        if(_emergencyDao == null) {
          _emergencyDao = new EmergencyDao_Impl(this);
        }
        return _emergencyDao;
      }
    }
  }
}
