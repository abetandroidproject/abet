package com.example.abeete3.databinding;
import com.example.abeete3.R;
import com.example.abeete3.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentRegistrationBindingImpl extends FragmentRegistrationBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.scroll, 1);
        sViewsWithIds.put(R.id.imageView3, 2);
        sViewsWithIds.put(R.id.textView9, 3);
        sViewsWithIds.put(R.id.divider2, 4);
        sViewsWithIds.put(R.id.textInputLayout2, 5);
        sViewsWithIds.put(R.id.FullName, 6);
        sViewsWithIds.put(R.id.lin, 7);
        sViewsWithIds.put(R.id.textInputLayout3, 8);
        sViewsWithIds.put(R.id.City, 9);
        sViewsWithIds.put(R.id.textInputLayout13, 10);
        sViewsWithIds.put(R.id.HouseNo, 11);
        sViewsWithIds.put(R.id.textInputLayout4, 12);
        sViewsWithIds.put(R.id.dOB, 13);
        sViewsWithIds.put(R.id.textView10, 14);
        sViewsWithIds.put(R.id.divider3, 15);
        sViewsWithIds.put(R.id.lin2, 16);
        sViewsWithIds.put(R.id.BloodType, 17);
        sViewsWithIds.put(R.id.textInputLayout15, 18);
        sViewsWithIds.put(R.id.Weight, 19);
        sViewsWithIds.put(R.id.textInputLayout17, 20);
        sViewsWithIds.put(R.id.Heigh, 21);
        sViewsWithIds.put(R.id.textInputLayout7, 22);
        sViewsWithIds.put(R.id.Allergy, 23);
        sViewsWithIds.put(R.id.textInputLayout9, 24);
        sViewsWithIds.put(R.id.MedicalConditions, 25);
        sViewsWithIds.put(R.id.textinputlayout11, 26);
        sViewsWithIds.put(R.id.Medications, 27);
        sViewsWithIds.put(R.id.registerButton, 28);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentRegistrationBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 29, sIncludes, sViewsWithIds));
    }
    private FragmentRegistrationBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.textfield.TextInputEditText) bindings[23]
            , (android.widget.Spinner) bindings[17]
            , (com.google.android.material.textfield.TextInputEditText) bindings[9]
            , (com.google.android.material.textfield.TextInputEditText) bindings[6]
            , (com.google.android.material.textfield.TextInputEditText) bindings[21]
            , (com.google.android.material.textfield.TextInputEditText) bindings[11]
            , (com.google.android.material.textfield.TextInputEditText) bindings[25]
            , (com.google.android.material.textfield.TextInputEditText) bindings[27]
            , (com.google.android.material.textfield.TextInputEditText) bindings[19]
            , (com.google.android.material.textfield.TextInputEditText) bindings[13]
            , (android.view.View) bindings[4]
            , (android.view.View) bindings[15]
            , (android.widget.ImageView) bindings[2]
            , (android.widget.LinearLayout) bindings[7]
            , (android.widget.LinearLayout) bindings[16]
            , (com.google.android.material.button.MaterialButton) bindings[28]
            , (android.widget.ScrollView) bindings[1]
            , (com.google.android.material.textfield.TextInputLayout) bindings[10]
            , (com.google.android.material.textfield.TextInputLayout) bindings[18]
            , (com.google.android.material.textfield.TextInputLayout) bindings[20]
            , (com.google.android.material.textfield.TextInputLayout) bindings[5]
            , (com.google.android.material.textfield.TextInputLayout) bindings[8]
            , (com.google.android.material.textfield.TextInputLayout) bindings[12]
            , (com.google.android.material.textfield.TextInputLayout) bindings[22]
            , (com.google.android.material.textfield.TextInputLayout) bindings[24]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[3]
            , (com.google.android.material.textfield.TextInputLayout) bindings[26]
            );
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.toProfile == variableId) {
            setToProfile((com.example.abeete3.viewmodel.UserViewModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setToProfile(@Nullable com.example.abeete3.viewmodel.UserViewModel ToProfile) {
        this.mToProfile = ToProfile;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): toProfile
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}