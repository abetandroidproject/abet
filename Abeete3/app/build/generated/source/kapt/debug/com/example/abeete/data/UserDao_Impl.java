package com.example.abeete.data;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.example.abeete3.data.User;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class UserDao_Impl implements UserDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfUser;

  public UserDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfUser = new EntityInsertionAdapter<User>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `user`(`name`,`city`,`houseno`,`height`,`weight`,`bloodType`,`dob`,`allergies`,`medicalConditions`,`medications`) VALUES (?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, User value) {
        if (value.getName() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getName());
        }
        if (value.getCity() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getCity());
        }
        if (value.getHouseno() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getHouseno());
        }
        stmt.bindDouble(4, value.getHeight());
        stmt.bindDouble(5, value.getWeight());
        if (value.getBloodType() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getBloodType());
        }
        if (value.getDob() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getDob());
        }
        if (value.getAllergies() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getAllergies());
        }
        if (value.getMedicalConditions() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getMedicalConditions());
        }
        if (value.getMedications() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getMedications());
        }
      }
    };
  }

  @Override
  public long insertUser(final User user) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfUser.insertAndReturnId(user);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<User> AllUsers() {
    final String _sql = "SELECT * FROM user ORDER BY name";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"user"}, false, new Callable<User>() {
      @Override
      public User call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
          final int _cursorIndexOfCity = CursorUtil.getColumnIndexOrThrow(_cursor, "city");
          final int _cursorIndexOfHouseno = CursorUtil.getColumnIndexOrThrow(_cursor, "houseno");
          final int _cursorIndexOfHeight = CursorUtil.getColumnIndexOrThrow(_cursor, "height");
          final int _cursorIndexOfWeight = CursorUtil.getColumnIndexOrThrow(_cursor, "weight");
          final int _cursorIndexOfBloodType = CursorUtil.getColumnIndexOrThrow(_cursor, "bloodType");
          final int _cursorIndexOfDob = CursorUtil.getColumnIndexOrThrow(_cursor, "dob");
          final int _cursorIndexOfAllergies = CursorUtil.getColumnIndexOrThrow(_cursor, "allergies");
          final int _cursorIndexOfMedicalConditions = CursorUtil.getColumnIndexOrThrow(_cursor, "medicalConditions");
          final int _cursorIndexOfMedications = CursorUtil.getColumnIndexOrThrow(_cursor, "medications");
          final User _result;
          if(_cursor.moveToFirst()) {
            final String _tmpName;
            _tmpName = _cursor.getString(_cursorIndexOfName);
            final String _tmpCity;
            _tmpCity = _cursor.getString(_cursorIndexOfCity);
            final String _tmpHouseno;
            _tmpHouseno = _cursor.getString(_cursorIndexOfHouseno);
            final double _tmpHeight;
            _tmpHeight = _cursor.getDouble(_cursorIndexOfHeight);
            final double _tmpWeight;
            _tmpWeight = _cursor.getDouble(_cursorIndexOfWeight);
            final String _tmpBloodType;
            _tmpBloodType = _cursor.getString(_cursorIndexOfBloodType);
            final String _tmpDob;
            _tmpDob = _cursor.getString(_cursorIndexOfDob);
            final String _tmpAllergies;
            _tmpAllergies = _cursor.getString(_cursorIndexOfAllergies);
            final String _tmpMedicalConditions;
            _tmpMedicalConditions = _cursor.getString(_cursorIndexOfMedicalConditions);
            final String _tmpMedications;
            _tmpMedications = _cursor.getString(_cursorIndexOfMedications);
            _result = new User(_tmpName,_tmpCity,_tmpHouseno,_tmpHeight,_tmpWeight,_tmpBloodType,_tmpDob,_tmpAllergies,_tmpMedicalConditions,_tmpMedications);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<User> getUserByName(final String name) {
    final String _sql = "Select * from user where name=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (name == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, name);
    }
    return __db.getInvalidationTracker().createLiveData(new String[]{"user"}, false, new Callable<User>() {
      @Override
      public User call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
          final int _cursorIndexOfCity = CursorUtil.getColumnIndexOrThrow(_cursor, "city");
          final int _cursorIndexOfHouseno = CursorUtil.getColumnIndexOrThrow(_cursor, "houseno");
          final int _cursorIndexOfHeight = CursorUtil.getColumnIndexOrThrow(_cursor, "height");
          final int _cursorIndexOfWeight = CursorUtil.getColumnIndexOrThrow(_cursor, "weight");
          final int _cursorIndexOfBloodType = CursorUtil.getColumnIndexOrThrow(_cursor, "bloodType");
          final int _cursorIndexOfDob = CursorUtil.getColumnIndexOrThrow(_cursor, "dob");
          final int _cursorIndexOfAllergies = CursorUtil.getColumnIndexOrThrow(_cursor, "allergies");
          final int _cursorIndexOfMedicalConditions = CursorUtil.getColumnIndexOrThrow(_cursor, "medicalConditions");
          final int _cursorIndexOfMedications = CursorUtil.getColumnIndexOrThrow(_cursor, "medications");
          final User _result;
          if(_cursor.moveToFirst()) {
            final String _tmpName;
            _tmpName = _cursor.getString(_cursorIndexOfName);
            final String _tmpCity;
            _tmpCity = _cursor.getString(_cursorIndexOfCity);
            final String _tmpHouseno;
            _tmpHouseno = _cursor.getString(_cursorIndexOfHouseno);
            final double _tmpHeight;
            _tmpHeight = _cursor.getDouble(_cursorIndexOfHeight);
            final double _tmpWeight;
            _tmpWeight = _cursor.getDouble(_cursorIndexOfWeight);
            final String _tmpBloodType;
            _tmpBloodType = _cursor.getString(_cursorIndexOfBloodType);
            final String _tmpDob;
            _tmpDob = _cursor.getString(_cursorIndexOfDob);
            final String _tmpAllergies;
            _tmpAllergies = _cursor.getString(_cursorIndexOfAllergies);
            final String _tmpMedicalConditions;
            _tmpMedicalConditions = _cursor.getString(_cursorIndexOfMedicalConditions);
            final String _tmpMedications;
            _tmpMedications = _cursor.getString(_cursorIndexOfMedications);
            _result = new User(_tmpName,_tmpCity,_tmpHouseno,_tmpHeight,_tmpWeight,_tmpBloodType,_tmpDob,_tmpAllergies,_tmpMedicalConditions,_tmpMedications);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }
}
