package com.example.abeete.data;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.example.abeete3.data.EmergencyContact;
import java.lang.Exception;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class EmergencyDao_Impl implements EmergencyDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter __insertionAdapterOfEmergencyContact;

  public EmergencyDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfEmergencyContact = new EntityInsertionAdapter<EmergencyContact>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `EmergencyContact`(`name`,`phoneNumber`,`relationShip`) VALUES (?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, EmergencyContact value) {
        if (value.getName() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getName());
        }
        if (value.getPhoneNumber() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getPhoneNumber());
        }
        if (value.getRelationShip() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getRelationShip());
        }
      }
    };
  }

  @Override
  public long insertContact(final EmergencyContact contact) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfEmergencyContact.insertAndReturnId(contact);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public LiveData<EmergencyContact> getContact(final String name) {
    final String _sql = "SELECT * FROM EmergencyContact WHERE name=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (name == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, name);
    }
    return __db.getInvalidationTracker().createLiveData(new String[]{"EmergencyContact"}, false, new Callable<EmergencyContact>() {
      @Override
      public EmergencyContact call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
          final int _cursorIndexOfPhoneNumber = CursorUtil.getColumnIndexOrThrow(_cursor, "phoneNumber");
          final int _cursorIndexOfRelationShip = CursorUtil.getColumnIndexOrThrow(_cursor, "relationShip");
          final EmergencyContact _result;
          if(_cursor.moveToFirst()) {
            final String _tmpName;
            _tmpName = _cursor.getString(_cursorIndexOfName);
            final String _tmpPhoneNumber;
            _tmpPhoneNumber = _cursor.getString(_cursorIndexOfPhoneNumber);
            final String _tmpRelationShip;
            _tmpRelationShip = _cursor.getString(_cursorIndexOfRelationShip);
            _result = new EmergencyContact(_tmpName,_tmpPhoneNumber,_tmpRelationShip);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<EmergencyContact> allContacts() {
    final String _sql = "SELECT * FROM EmergencyContact";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"EmergencyContact"}, false, new Callable<EmergencyContact>() {
      @Override
      public EmergencyContact call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false);
        try {
          final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
          final int _cursorIndexOfPhoneNumber = CursorUtil.getColumnIndexOrThrow(_cursor, "phoneNumber");
          final int _cursorIndexOfRelationShip = CursorUtil.getColumnIndexOrThrow(_cursor, "relationShip");
          final EmergencyContact _result;
          if(_cursor.moveToFirst()) {
            final String _tmpName;
            _tmpName = _cursor.getString(_cursorIndexOfName);
            final String _tmpPhoneNumber;
            _tmpPhoneNumber = _cursor.getString(_cursorIndexOfPhoneNumber);
            final String _tmpRelationShip;
            _tmpRelationShip = _cursor.getString(_cursorIndexOfRelationShip);
            _result = new EmergencyContact(_tmpName,_tmpPhoneNumber,_tmpRelationShip);
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }
}
