package com.example.abeete3.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.VideoView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.abeete3.data.FirstAid;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentVideoBinding extends ViewDataBinding {
  @NonNull
  public final TextView textView24;

  @NonNull
  public final TextView title;

  @NonNull
  public final VideoView videoView;

  @Bindable
  protected FirstAid mFromApi;

  protected FragmentVideoBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView textView24, TextView title, VideoView videoView) {
    super(_bindingComponent, _root, _localFieldCount);
    this.textView24 = textView24;
    this.title = title;
    this.videoView = videoView;
  }

  public abstract void setFromApi(@Nullable FirstAid fromApi);

  @Nullable
  public FirstAid getFromApi() {
    return mFromApi;
  }

  @NonNull
  public static FragmentVideoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_video, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentVideoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentVideoBinding>inflateInternal(inflater, com.example.abeete3.R.layout.fragment_video, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentVideoBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_video, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentVideoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentVideoBinding>inflateInternal(inflater, com.example.abeete3.R.layout.fragment_video, null, false, component);
  }

  public static FragmentVideoBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentVideoBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentVideoBinding)bind(component, view, com.example.abeete3.R.layout.fragment_video);
  }
}
