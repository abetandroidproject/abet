package com.example.abeete3.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.abeete3.data.FirstAid;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentImageBinding extends ViewDataBinding {
  @NonNull
  public final ImageView imageView2;

  @NonNull
  public final TextView textView30;

  @NonNull
  public final TextView title;

  @Bindable
  protected FirstAid mFromApi;

  protected FragmentImageBinding(Object _bindingComponent, View _root, int _localFieldCount,
      ImageView imageView2, TextView textView30, TextView title) {
    super(_bindingComponent, _root, _localFieldCount);
    this.imageView2 = imageView2;
    this.textView30 = textView30;
    this.title = title;
  }

  public abstract void setFromApi(@Nullable FirstAid fromApi);

  @Nullable
  public FirstAid getFromApi() {
    return mFromApi;
  }

  @NonNull
  public static FragmentImageBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_image, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentImageBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentImageBinding>inflateInternal(inflater, com.example.abeete3.R.layout.fragment_image, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentImageBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_image, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentImageBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentImageBinding>inflateInternal(inflater, com.example.abeete3.R.layout.fragment_image, null, false, component);
  }

  public static FragmentImageBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentImageBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentImageBinding)bind(component, view, com.example.abeete3.R.layout.fragment_image);
  }
}
