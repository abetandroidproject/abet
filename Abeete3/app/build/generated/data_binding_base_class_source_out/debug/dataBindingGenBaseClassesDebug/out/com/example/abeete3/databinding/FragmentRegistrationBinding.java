package com.example.abeete3.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.abeete3.viewmodel.UserViewModel;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentRegistrationBinding extends ViewDataBinding {
  @NonNull
  public final TextInputEditText Allergy;

  @NonNull
  public final Spinner BloodType;

  @NonNull
  public final TextInputEditText City;

  @NonNull
  public final TextInputEditText FullName;

  @NonNull
  public final TextInputEditText Heigh;

  @NonNull
  public final TextInputEditText HouseNo;

  @NonNull
  public final TextInputEditText MedicalConditions;

  @NonNull
  public final TextInputEditText Medications;

  @NonNull
  public final TextInputEditText Weight;

  @NonNull
  public final TextInputEditText dOB;

  @NonNull
  public final View divider2;

  @NonNull
  public final View divider3;

  @NonNull
  public final ImageView imageView3;

  @NonNull
  public final LinearLayout lin;

  @NonNull
  public final LinearLayout lin2;

  @NonNull
  public final MaterialButton registerButton;

  @NonNull
  public final ScrollView scroll;

  @NonNull
  public final TextInputLayout textInputLayout13;

  @NonNull
  public final TextInputLayout textInputLayout15;

  @NonNull
  public final TextInputLayout textInputLayout17;

  @NonNull
  public final TextInputLayout textInputLayout2;

  @NonNull
  public final TextInputLayout textInputLayout3;

  @NonNull
  public final TextInputLayout textInputLayout4;

  @NonNull
  public final TextInputLayout textInputLayout7;

  @NonNull
  public final TextInputLayout textInputLayout9;

  @NonNull
  public final TextView textView10;

  @NonNull
  public final TextView textView9;

  @NonNull
  public final TextInputLayout textinputlayout11;

  @Bindable
  protected UserViewModel mToProfile;

  protected FragmentRegistrationBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextInputEditText Allergy, Spinner BloodType, TextInputEditText City,
      TextInputEditText FullName, TextInputEditText Heigh, TextInputEditText HouseNo,
      TextInputEditText MedicalConditions, TextInputEditText Medications, TextInputEditText Weight,
      TextInputEditText dOB, View divider2, View divider3, ImageView imageView3, LinearLayout lin,
      LinearLayout lin2, MaterialButton registerButton, ScrollView scroll,
      TextInputLayout textInputLayout13, TextInputLayout textInputLayout15,
      TextInputLayout textInputLayout17, TextInputLayout textInputLayout2,
      TextInputLayout textInputLayout3, TextInputLayout textInputLayout4,
      TextInputLayout textInputLayout7, TextInputLayout textInputLayout9, TextView textView10,
      TextView textView9, TextInputLayout textinputlayout11) {
    super(_bindingComponent, _root, _localFieldCount);
    this.Allergy = Allergy;
    this.BloodType = BloodType;
    this.City = City;
    this.FullName = FullName;
    this.Heigh = Heigh;
    this.HouseNo = HouseNo;
    this.MedicalConditions = MedicalConditions;
    this.Medications = Medications;
    this.Weight = Weight;
    this.dOB = dOB;
    this.divider2 = divider2;
    this.divider3 = divider3;
    this.imageView3 = imageView3;
    this.lin = lin;
    this.lin2 = lin2;
    this.registerButton = registerButton;
    this.scroll = scroll;
    this.textInputLayout13 = textInputLayout13;
    this.textInputLayout15 = textInputLayout15;
    this.textInputLayout17 = textInputLayout17;
    this.textInputLayout2 = textInputLayout2;
    this.textInputLayout3 = textInputLayout3;
    this.textInputLayout4 = textInputLayout4;
    this.textInputLayout7 = textInputLayout7;
    this.textInputLayout9 = textInputLayout9;
    this.textView10 = textView10;
    this.textView9 = textView9;
    this.textinputlayout11 = textinputlayout11;
  }

  public abstract void setToProfile(@Nullable UserViewModel toProfile);

  @Nullable
  public UserViewModel getToProfile() {
    return mToProfile;
  }

  @NonNull
  public static FragmentRegistrationBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_registration, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentRegistrationBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentRegistrationBinding>inflateInternal(inflater, com.example.abeete3.R.layout.fragment_registration, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentRegistrationBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_registration, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentRegistrationBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentRegistrationBinding>inflateInternal(inflater, com.example.abeete3.R.layout.fragment_registration, null, false, component);
  }

  public static FragmentRegistrationBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentRegistrationBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentRegistrationBinding)bind(component, view, com.example.abeete3.R.layout.fragment_registration);
  }
}
