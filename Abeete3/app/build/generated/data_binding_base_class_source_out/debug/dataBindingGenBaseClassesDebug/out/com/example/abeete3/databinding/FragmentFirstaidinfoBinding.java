package com.example.abeete3.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentFirstaidinfoBinding extends ViewDataBinding {
  @NonNull
  public final AppBarLayout appbap2;

  @NonNull
  public final SearchView search;

  @NonNull
  public final TabLayout tabs;

  @NonNull
  public final ViewPager viewpager;

  protected FragmentFirstaidinfoBinding(Object _bindingComponent, View _root, int _localFieldCount,
      AppBarLayout appbap2, SearchView search, TabLayout tabs, ViewPager viewpager) {
    super(_bindingComponent, _root, _localFieldCount);
    this.appbap2 = appbap2;
    this.search = search;
    this.tabs = tabs;
    this.viewpager = viewpager;
  }

  @NonNull
  public static FragmentFirstaidinfoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_firstaidinfo, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentFirstaidinfoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentFirstaidinfoBinding>inflateInternal(inflater, com.example.abeete3.R.layout.fragment_firstaidinfo, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentFirstaidinfoBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_firstaidinfo, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentFirstaidinfoBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentFirstaidinfoBinding>inflateInternal(inflater, com.example.abeete3.R.layout.fragment_firstaidinfo, null, false, component);
  }

  public static FragmentFirstaidinfoBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentFirstaidinfoBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentFirstaidinfoBinding)bind(component, view, com.example.abeete3.R.layout.fragment_firstaidinfo);
  }
}
