package com.example.abeete3.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.example.abeete3.data.FirstAid;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentTextBinding extends ViewDataBinding {
  @NonNull
  public final TextView medication;

  @NonNull
  public final TextView sympthoms;

  @NonNull
  public final TextView textView17;

  @NonNull
  public final TextView textView25;

  @NonNull
  public final TextView textView27;

  @NonNull
  public final TextView title;

  @Bindable
  protected FirstAid mFromApi;

  protected FragmentTextBinding(Object _bindingComponent, View _root, int _localFieldCount,
      TextView medication, TextView sympthoms, TextView textView17, TextView textView25,
      TextView textView27, TextView title) {
    super(_bindingComponent, _root, _localFieldCount);
    this.medication = medication;
    this.sympthoms = sympthoms;
    this.textView17 = textView17;
    this.textView25 = textView25;
    this.textView27 = textView27;
    this.title = title;
  }

  public abstract void setFromApi(@Nullable FirstAid fromApi);

  @Nullable
  public FirstAid getFromApi() {
    return mFromApi;
  }

  @NonNull
  public static FragmentTextBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_text, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentTextBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentTextBinding>inflateInternal(inflater, com.example.abeete3.R.layout.fragment_text, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentTextBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_text, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentTextBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentTextBinding>inflateInternal(inflater, com.example.abeete3.R.layout.fragment_text, null, false, component);
  }

  public static FragmentTextBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentTextBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentTextBinding)bind(component, view, com.example.abeete3.R.layout.fragment_text);
  }
}
