package com.FirstAid.FirstAid.Domain;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;

import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;


@Data
@Entity

public class User {
	@Id

    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
	private String name;
	private String city;
	private String houseno;
	private String height;
	private Long weight;
	private String bloodType;
	private String dob;
	private String allergies;
	private String medicalConditions;
	private String medications;
	
	

}
