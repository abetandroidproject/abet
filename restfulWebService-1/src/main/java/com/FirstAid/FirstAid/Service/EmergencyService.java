package com.FirstAid.FirstAid.Service;

import java.util.Optional;

import com.FirstAid.FirstAid.Domain.EmergencyContacts;

public interface EmergencyService {
	
	public EmergencyContacts save(EmergencyContacts emergencyContacts);
	
	public Iterable<EmergencyContacts> saveAll(Iterable<EmergencyContacts> emergencyContacts);

	Optional<EmergencyContacts> findById(Long id);
	Optional<EmergencyContacts> findByName(String name);

	boolean existsById(Long id);
	
	Iterable<EmergencyContacts> findAll();

	Iterable<EmergencyContacts> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(EmergencyContacts emergencyContacts);
	
	void deleteAll(Iterable<EmergencyContacts> emergencyContacts);

	void deleteAll();
}
