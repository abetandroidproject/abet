package com.FirstAid.FirstAid.Service;

import java.util.Optional;

import com.FirstAid.FirstAid.Domain.FirstAidInfo;

public interface FirstAidService {
	
	public FirstAidInfo save(FirstAidInfo info);
	
	public Iterable<FirstAidInfo> saveAll(Iterable<FirstAidInfo> info);

	Optional<FirstAidInfo> findById(Long id);
	Optional<FirstAidInfo> findByTitle(String title);

	boolean existsById(Long id);
	
	Iterable<FirstAidInfo> findAll();

	Iterable<FirstAidInfo> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(FirstAidInfo info);
	
	void deleteAll(Iterable<FirstAidInfo> info);

	void deleteAll();
}
