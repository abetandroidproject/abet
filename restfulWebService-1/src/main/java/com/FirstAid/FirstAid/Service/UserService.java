package com.FirstAid.FirstAid.Service;

import java.util.Optional;

import com.FirstAid.FirstAid.Domain.User;

public interface UserService {
	
	public User save(User user);
	public Optional<User> findByName(String name);
	public void deleteByName(String name);
	public Iterable<User> saveAll(Iterable<User> user);

	Optional<User> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<User> findAll();

	Iterable<User> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(User user);
	
	void deleteAll(Iterable<User> user);

	void deleteAll();
}
