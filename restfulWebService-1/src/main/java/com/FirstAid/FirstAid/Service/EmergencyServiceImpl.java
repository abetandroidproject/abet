package com.FirstAid.FirstAid.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FirstAid.FirstAid.Domain.EmergencyContacts;
import com.FirstAid.FirstAid.Repository.EmergencyRepository;

@Service
public class EmergencyServiceImpl implements EmergencyService{

	private EmergencyRepository emergencyRepository;
	
	@Autowired
	public EmergencyServiceImpl(EmergencyRepository emergencyRepository)
	{
		this.emergencyRepository = emergencyRepository;
	}

	@Override
	public EmergencyContacts save(EmergencyContacts emergencyContacts) {
		return emergencyRepository.save(emergencyContacts);
	}

	@Override
	public Iterable<EmergencyContacts> saveAll(Iterable<EmergencyContacts> emergencyContacts) {
		return emergencyRepository.saveAll(emergencyContacts);
	}

	@Override
	public Optional<EmergencyContacts> findById(Long id) {
		return emergencyRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return emergencyRepository.existsById(id);
	}

	@Override
	public Iterable<EmergencyContacts> findAll() {
		return emergencyRepository.findAll();
	}

	@Override
	public Iterable<EmergencyContacts> findAllById(Iterable<Long> ids) {
		return emergencyRepository.findAllById(ids);
	}


	@Override
	public long count() {
		return emergencyRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		emergencyRepository.deleteById(id);
		
	}

	@Override
	public void delete(EmergencyContacts emergencyContacts) {
		emergencyRepository.delete(emergencyContacts);
	}

	@Override
	public void deleteAll(Iterable<EmergencyContacts> emergencyContacts) {
		emergencyRepository.deleteAll(emergencyContacts);
		
	}

	@Override
	public void deleteAll() {
		emergencyRepository.deleteAll();
	}

	@Override
	public Optional<EmergencyContacts> findByName(String name) {
		
		return emergencyRepository.findByName(name);
	}
}
