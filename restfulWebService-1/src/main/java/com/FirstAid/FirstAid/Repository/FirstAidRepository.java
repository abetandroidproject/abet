package com.FirstAid.FirstAid.Repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.FirstAid.FirstAid.Domain.FirstAidInfo;
import com.FirstAid.FirstAid.Domain.User;

public interface FirstAidRepository extends CrudRepository<FirstAidInfo ,Long> {
	Optional<FirstAidInfo> findByTitle(String name);
}
