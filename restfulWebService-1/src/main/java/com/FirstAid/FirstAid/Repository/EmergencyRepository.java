package com.FirstAid.FirstAid.Repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.FirstAid.FirstAid.Domain.EmergencyContacts;
import com.FirstAid.FirstAid.Domain.User;

public interface EmergencyRepository extends CrudRepository<EmergencyContacts ,Long> {
	Optional<EmergencyContacts> findByName(String name);
}
