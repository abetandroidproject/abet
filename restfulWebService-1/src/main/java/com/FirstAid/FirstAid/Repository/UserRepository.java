package com.FirstAid.FirstAid.Repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.FirstAid.FirstAid.Domain.User;


public interface UserRepository extends CrudRepository<User ,Long> {
	Optional<User> findByName(String name);
	void deleteByName(String name);
}
