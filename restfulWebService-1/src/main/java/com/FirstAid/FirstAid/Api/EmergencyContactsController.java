package com.FirstAid.FirstAid.Api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.FirstAid.FirstAid.Domain.EmergencyContacts;
import com.FirstAid.FirstAid.Domain.User;
import com.FirstAid.FirstAid.Repository.EmergencyRepository;
import com.FirstAid.FirstAid.Service.EmergencyService;



@RestController
@RequestMapping(value="contact", produces = "application/json")
public class EmergencyContactsController {
@Autowired	
private EmergencyService emergencyService;
	@Autowired
	public EmergencyContactsController(EmergencyService emergencyService) {
		this.emergencyService = emergencyService;
	}
	@GetMapping("/all")
	public Iterable<EmergencyContacts> allInfo(){
		
		return emergencyService.findAll();
	}
	@GetMapping("/{id}")
	public ResponseEntity<EmergencyContacts> infoById(@PathVariable("id") Long id) {
		Optional<EmergencyContacts> emergencyContacts = emergencyService.findById(id);
		if(emergencyContacts.isPresent()) {
			return new ResponseEntity<>(emergencyContacts.get(),HttpStatus.OK);
		}
		return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
	}
	@GetMapping("/{name}")
	public ResponseEntity<EmergencyContacts> infoByName(@PathVariable("name") String name) {
		Optional<EmergencyContacts> emergencyContacts = emergencyService.findByName(name);
		if(emergencyContacts.isPresent()) {
			return new ResponseEntity<>(emergencyContacts.get(),HttpStatus.OK);
		}
		return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
	}

	

	@PostMapping(consumes="application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public EmergencyContacts saveInfo(@RequestBody EmergencyContacts emergencyContacts) {
		return emergencyService.save(emergencyContacts);
	}
	@PutMapping(path="/{id}", consumes="application/json")
	public EmergencyContacts updateInfo(@RequestBody EmergencyContacts emergencyContacts) {
		return emergencyService.save(emergencyContacts);
	}

	@DeleteMapping(path="/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteInfo(@PathVariable("id") Long id) {
		try {
			emergencyService.deleteById(id);
		}catch(EmptyResultDataAccessException e) {
			
		}
	}
}
 
