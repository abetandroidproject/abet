package com.FirstAid.FirstAid.Api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.FirstAid.FirstAid.Domain.User;
import com.FirstAid.FirstAid.Repository.UserRepository;
import com.FirstAid.FirstAid.Service.UserService;



@RestController
@RequestMapping(path="user", produces = "application/json")
public class UserController {
@Autowired	
private UserService userService;
	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}
	@GetMapping("/all")
	public Iterable<User> allInfo(){
		
		return userService.findAll();
	}
	@GetMapping("/{name}")
	public ResponseEntity<User> infoByName(@PathVariable("name") String name) {
		Optional<User> user = userService.findByName(name);
		if(user.isPresent()) {
			return new ResponseEntity<>(user.get(),HttpStatus.OK);
		}
		return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
	}

	

	@PostMapping(consumes="application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public User saveUser(@RequestBody User user) {
		return userService.save(user);
	}
	@PutMapping(path="/{name}", consumes="application/json")
	public User updateUser(@RequestBody User user) {
		return userService.save(user);
	}

	@DeleteMapping(path="/{name}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteInfo(@PathVariable("name") String name) {
		try {
			userService.deleteByName(name);
		}catch(EmptyResultDataAccessException e) {
			
		}
	}
}
 
