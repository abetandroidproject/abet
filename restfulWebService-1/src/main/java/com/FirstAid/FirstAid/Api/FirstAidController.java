package com.FirstAid.FirstAid.Api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.FirstAid.FirstAid.Domain.FirstAidInfo;
import com.FirstAid.FirstAid.Repository.FirstAidRepository;
import com.FirstAid.FirstAid.Service.FirstAidService;



@RestController
@RequestMapping(path="info", produces = "application/json")
public class FirstAidController {
@Autowired	
private FirstAidService firstAidService;
	@Autowired
	public FirstAidController(FirstAidService firstAidService) {
		this.firstAidService = firstAidService;
	}
	@GetMapping("/all")
	public Iterable<FirstAidInfo> allInfo(){
		
		return firstAidService.findAll();
	}
	@GetMapping("/{id}")
	public ResponseEntity<FirstAidInfo> infoById(@PathVariable("id") Long id) {
		Optional<FirstAidInfo> firstAidInfo = firstAidService.findById(id);
		if(firstAidInfo.isPresent()) {
			return new ResponseEntity<>(firstAidInfo.get(),HttpStatus.OK);
		}
		return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
	}
	@GetMapping("/{title}")
	public ResponseEntity<FirstAidInfo> infoByTitle(@PathVariable("title") String title) {
		Optional<FirstAidInfo> firstAidInfo = firstAidService.findByTitle(title);
		if(firstAidInfo.isPresent()) {
			return new ResponseEntity<>(firstAidInfo.get(),HttpStatus.OK);
		}
		return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
	}

	

	@PostMapping(consumes="application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public FirstAidInfo saveInfo(@RequestBody FirstAidInfo firstAidInfo) {
		return firstAidService.save(firstAidInfo);
	}
	@PutMapping(path="/{id}", consumes="application/json")
	public FirstAidInfo updateInfo(@RequestBody FirstAidInfo firstAidInfo) {
		return firstAidService.save(firstAidInfo);
	}

	@DeleteMapping(path="/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteInfo(@PathVariable("id") Long id) {
		try {
			firstAidService.deleteById(id);
		}catch(EmptyResultDataAccessException e) {
			
		}
	}
}
 
