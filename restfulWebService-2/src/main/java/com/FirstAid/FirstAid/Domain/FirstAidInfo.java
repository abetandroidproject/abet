package com.FirstAid.FirstAid.Domain;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;

import javax.persistence.GenerationType;

import javax.persistence.Id;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;


@Data
@Entity

public class FirstAidInfo {
	@Id

    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Long id;
	private String title;
	private String medication;
	private String sympthoms;
	private byte[] image;
	private byte[] video;
   

}
