package com.FirstAid.FirstAid.Service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.FirstAid.FirstAid.Domain.FirstAidInfo;
import com.FirstAid.FirstAid.Repository.FirstAidRepository;

@Service
public class FirstAidServiceImpl implements FirstAidService{

	private FirstAidRepository firstAidRepository;
	
	@Autowired
	public FirstAidServiceImpl(FirstAidRepository firstAidRepository)
	{
		this.firstAidRepository = firstAidRepository;
	}

	@Override
	public FirstAidInfo save(FirstAidInfo info) {
		return firstAidRepository.save(info);
	}

	@Override
	public Iterable<FirstAidInfo> saveAll(Iterable<FirstAidInfo> info) {
		return firstAidRepository.saveAll(info);
	}

	@Override
	public Optional<FirstAidInfo> findById(Long id) {
		return firstAidRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return firstAidRepository.existsById(id);
	}

	@Override
	public Iterable<FirstAidInfo> findAll() {
		return firstAidRepository.findAll();
	}

	@Override
	public Iterable<FirstAidInfo> findAllById(Iterable<Long> ids) {
		return firstAidRepository.findAllById(ids);
	}


	@Override
	public long count() {
		return firstAidRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		firstAidRepository.deleteById(id);
		
	}

	@Override
	public void delete(FirstAidInfo info) {
		firstAidRepository.delete(info);
	}

	@Override
	public void deleteAll(Iterable<FirstAidInfo> info) {
		firstAidRepository.deleteAll(info);
		
	}

	@Override
	public void deleteAll() {
		firstAidRepository.deleteAll();
	}
}
