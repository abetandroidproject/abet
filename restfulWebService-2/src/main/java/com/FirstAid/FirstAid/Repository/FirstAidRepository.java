package com.FirstAid.FirstAid.Repository;

import org.springframework.data.repository.CrudRepository;

import com.FirstAid.FirstAid.Domain.FirstAidInfo;

public interface FirstAidRepository extends CrudRepository<FirstAidInfo ,Long> {

}
